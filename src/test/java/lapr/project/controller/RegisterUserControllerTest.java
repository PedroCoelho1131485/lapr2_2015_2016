/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ricardo Pinto
 */
public class RegisterUserControllerTest {
    
    public RegisterUserControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of checkPassword method, of class RegisterUserController.
     */
    @Test
    public void testCheckPassword() {
        System.out.println("checkPassword");
        String string = "123abcDEF-.,";
        char[] charPassword = string.toCharArray();
        boolean expResult = true;
        boolean result = RegisterUserController.checkPassword(charPassword);
        assertEquals(expResult, result);
    }
    
}
