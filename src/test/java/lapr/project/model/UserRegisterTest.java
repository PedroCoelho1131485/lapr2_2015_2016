/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ricardo Pinto
 */
public class UserRegisterTest {
    
    public UserRegisterTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getUserList method, of class UserRegister.
     */
    @Test
    public void testGetUserList() {
        System.out.println("getUserList");
        User u = new User("rui", "rui email", "dragonlord666", "shhhhhh");
        ArrayList<User> expResult = new ArrayList<User>();
        UserRegister instance = new UserRegister();
        expResult.add(u);
        instance.addUser(u);
        ArrayList<User> result = instance.getUserList();
        assertEquals(expResult, result);
    }

    /**
     * Test of getUser method, of class UserRegister.
     */
    @Test
    public void testGetUser() {
        System.out.println("getUser");
        User u = new User("rui", "rui email", "dragonlord666", "shhhhhh");
        int index = 0;
        UserRegister instance = new UserRegister();
        User expResult = u;
        instance.addUser(u);
        User result = instance.getUser(index);
        assertEquals(expResult, result);   
    }

    /**
     * Test of addUser method, of class UserRegister.
     */
    @Test
    public void testAddUser() {
        System.out.println("addUser");
        User u = new User("rui", "rui email", "dragonlord666", "shhhhhh");
        UserRegister instance = new UserRegister();
        instance.addUser(u);
        instance.addUser(u);
        assertEquals(1,instance.getUserList().size());
    }

    /**
     * Test of removeFAE method, of class UserRegister.
     */
    @Test
    public void testRemoveFAE() {
        System.out.println("removeFAE");
        User u = new User("rui", "rui email", "dragonlord666", "shhhhhh");
        UserRegister instance = new UserRegister();
        instance.addUser(u);
        assertEquals(u,instance.getUser(0));
        instance.removeFAE(u);
        assertEquals(0,instance.getSize());
    }

    /**
     * Test of indexOf method, of class UserRegister.
     */
    @Test
    public void testIndexOf() {
        System.out.println("indexOf");
        User u = new User("rui", "rui email", "dragonlord666", "shhhhhh");
        UserRegister instance = new UserRegister();
        int expResult = 0;
        instance.addUser(u);
        int result = instance.indexOf(u);
        assertEquals(expResult, result);
    }

    /**
     * Test of getSize method, of class UserRegister.
     */
    @Test
    public void testGetSize() {
        System.out.println("getSize");
        UserRegister instance = new UserRegister();
        int expResult = 0;
        int result = instance.getSize();
        assertEquals(expResult, result);
        User u = new User("rui", "rui email", "dragonlord666", "shhhhhh");
        instance.addUser(u);
        expResult = 1;
        result = instance.getSize();
        assertEquals(expResult,result);
    }

    /**
     * Test of printUserList method, of class UserRegister.
     */
    @Test
    public void testPrintUserList() {
        System.out.println("printUserList");
        UserRegister instance = new UserRegister();
        instance.printUserList();
    }

    /**
     * Test of getOrganizerByUsername method, of class UserRegister.
     */
    @Test
    public void testGetOrganizerByUsername() {
        System.out.println("getOrganizerByUsername");
        String username = "dude";
        Organizer o = new Organizer(username,username,username,username);
        UserRegister instance = new UserRegister();
        int expResult = -1;
        int result = instance.getOrganizerByUsername(username);
        assertEquals(expResult,result);
        expResult = 0;
        instance.addUser(o);
        result = instance.getOrganizerByUsername(username);
        assertEquals(expResult, result);

    }
    
}
