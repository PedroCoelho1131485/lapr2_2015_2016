/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import static lapr.project.model.User.encrypt;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Manuel Sambade
 */
public class UserTest {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of hasID method, of class User.
     */
    @Test
    public void testHasID() {
        System.out.println("hasID");
        String id = "petershk";
        User instance = new User("Peter","peter_shmikel@sapo.pt","petershk","123");
        boolean expResult = true;
        boolean result = instance.hasID(id);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getID method, of class User.
     */
    @Test
    public void testGetID() {
        System.out.println("getID");
        User instance = new User("Peter","peter_shmikel@sapo.pt","petershk","123");
        String expResult = "petershk";
        String result = instance.getID();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getName method, of class User.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        User instance = new User("Peter","peter_shmikel@sapo.pt","petershk","123");;
        String expResult = "Peter";
        String result = instance.getName();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setName method, of class User.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "Peter";
        User instance = new User(name,"peter_shmikel@sapo.pt","petershk","123");;
        instance.setName(name);
        assertEquals(name,instance.getName());
    }

    /**
     * Test of getEmail method, of class User.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        User instance = new User("Peter","peter_shmiker@sapo.pt","petershk","123");
        String expResult = "peter_shmiker@sapo.pt";
        String result = instance.getEmail();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of setEmail method, of class User.
     */
    @Test
    public void testSetEmail() {
        System.out.println("setEmail");
        String email = "hello@gmail.com";
        User instance = new User("Peter",email,"petershk","123");
        instance.setEmail(email);
        assertEquals(email,instance.getEmail());
    }

    /**
     * Test of getUsername method, of class User.
     */
    @Test
    public void testGetUsername() {
        System.out.println("getUsername");
        User instance = new User("Peter","peter_shmikel@sapo.pt","petershk","123");
        String expResult = "petershk";
        String result = instance.getUsername();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setUsername method, of class User.
     */
    @Test
    public void testSetUsername() {
        System.out.println("setUsername");
        String username = "petershk";
        User instance = new User("Peter","peter_shmikel@sapo.pt",username,"123");
        instance.setUsername(username);
        assertEquals(username,instance.getUsername());
    }

    /**
     * Test of setPassword method, of class User.
     */
    @Test
    public void testSetPassword() {
        System.out.println("setPassword");
        String password = "123";
        User instance = new User("Peter","peter_shmikel@sapo.pt","petershk",password);
        instance.setPassword(password);
        assertEquals(password,instance.getHashedPassword());
    }

    /**
     * Test of toString method, of class User.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String name = "Peter";
        String email = "hello@gmail.com";
        String username = "petershk";
        String password = "123";
        String hashed = encrypt(password.toCharArray());
        User instance = new User(name,email,username,password);
        String expResult = "Name: " + name + ", email: " + email + ", userName: " 
                + username + ", password: " + hashed;;
        String result = instance.toString();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of equals method, of class User.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        User u = new User("peter","p@sapo.pt","Something","111");
        User instance = new User("ugo","uuu@ggg.com","User325","222");
        boolean expResult = false;
        boolean result = instance.equals(u);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getRole method, of class User.
     */
    @Test
    public void testGetRole() {
        System.out.println("getRole");
        User instance = new User("Peter","peter_shmikel@sapo.pt","petershk","123");
        String expResult = UserRole.USER;
        String result = instance.getRole();
        assertEquals(expResult, result);
    }
    
        /**
     * Test of encrypt method, of class RegisterUserController.
     */
    @Test
    public void testEncrypt() {
        System.out.println("encrypt");
        String string = "abc";
        char[] charPassword = string.toCharArray();
        String expResult = "def";
        String result = User.encrypt(charPassword);
        assertEquals(expResult, result);
    }

    /**
     * Test of getHashedPassword method, of class User.
     */
    @Test
    public void testGetHashedPassword() {
        System.out.println("getHashedPassword");
        User instance = new User("Peter","peter_shmikel@sapo.pt","petershk","123");
        String pass = "123";
        String expResult = encrypt(pass.toCharArray());
        String result = instance.getHashedPassword();
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class User.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        User instance = new User("organizer","organizer","organizer","organizer");
        int expResult = -616509464;
        int result = instance.hashCode();
        assertEquals(expResult, result);
        System.out.println(result);
    }
}
