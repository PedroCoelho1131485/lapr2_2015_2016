/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Manuel Sambade
 */
public class FAETest {

    @org.junit.BeforeClass
    public static void setUpClass() throws Exception {
    }

    @org.junit.AfterClass
    public static void tearDownClass() throws Exception {
    }

    @org.junit.Before
    public void setUp() throws Exception {
    }

    @org.junit.After
    public void tearDown() throws Exception {
    }

    private static FAE instance = new FAE("name", "email", "UserName", "Password");

    /**
     * Test of toString method, of class FAE.
     */
    @org.junit.Test
    public void testToString() {
        System.out.println("test toString");
        String expResult = String.format("FAE: name"); 
        String result = instance.toString();
        assertEquals(expResult, result);
         
    }

    /**
     * Test of getRole method, of class FAE.
     */
    @Test
    public void testGetRole() {
        System.out.println("getRole");
        String expResult = UserRole.FAE;
        String result = instance.getRole();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class FAE.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object o = null;
        FAE instance = new FAE("fae3","fae3@centro.pt","fae3@centro.pt","password");
        boolean expResult = false;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);
        FAE f = instance;
        expResult = true;
        result = instance.equals(f);
        assertEquals(expResult,result);
    }


    /**
     * Test of the method hashCode, of class FAE
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        FAE f = new FAE("fae1","fae1@centro.pt","fae1@centro.pt","password");
        int expResult = 7;
        int result = f.hashCode();
        assertEquals(expResult,result,0);
    }
    
}
