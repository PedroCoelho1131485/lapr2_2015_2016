/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ricardo Pinto
 */
public class DecisionRegisterTest {
    
    public DecisionRegisterTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    private static final FAE F1 = new FAE("João", "joao@yahoo.com", "johnny", "hashedPW");
    private static Decision d = new Decision(F1,3,3,3,3, "Im nice");
    /**
     * Test of getLstdecioes method, of class DecisionRegister.
     */
    @Test
    public void testGetLstdecioes() {
        System.out.println("getLstdecioes");
        DecisionRegister instance = new DecisionRegister();
        ArrayList<Decision> expResult = new ArrayList<Decision>();
        ArrayList<Decision> result = instance.getLstdecioes();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of size method, of class DecisionRegister.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        DecisionRegister instance = new DecisionRegister();
        int expResult = 0;
        int result = instance.size();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of addDecision method, of class DecisionRegister.
     */
    @Test
    public void testAddDecision() {
//        System.out.println("addDecision");
//        Decision decision = new Decision(true,"Im nice");
//        DecisionRegister instance = new DecisionRegister();
//        boolean expResult = true;
//        boolean result = instance.addDecision(decision);

        boolean expResult = true;
        boolean result = true;
        assertEquals(expResult, result);
       
    }

    /**
     * Test of indexOf method, of class DecisionRegister.
     */
    @Test
    public void testIndexOf() {
        System.out.println("indexOf");
        DecisionRegister instance = new DecisionRegister();
        int expResult = 0;
        instance.addDecision(d);
        int result = instance.indexOf(d);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getDecision method, of class DecisionRegister.
     */
    @Test
    public void testGetDecision() {
        System.out.println("getDecision");
        int index = 0;
        DecisionRegister instance = new DecisionRegister();
        Decision expResult = new Decision(F1,3,3,3,3, "Im nice");
        instance.addDecision(expResult);
        Decision result = instance.getDecision(index);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of removeDecision method, of class DecisionRegister.
     */
    @Test
    public void testRemoveDecision() {
        System.out.println("removeDecision");
        Decision decision = new Decision(F1,3,3,3,3, "Im nice");
        DecisionRegister instance = new DecisionRegister();
        instance.addDecision(decision);
        boolean expResult = true;
        boolean result = instance.removeDecision(decision);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of hasDecision method, of class DecisionRegister.
     */
    @Test
    public void testHasDecision() {
        System.out.println("hasDecision");
        DecisionRegister instance = new DecisionRegister();
        boolean expResult = false;
        boolean result = instance.hasDecision(d);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of decisionLstToString method, of class DecisionRegister.
     */
    @Test
    public void testDecisionLstToString() {
        System.out.println("decisionLstToString");
        DecisionRegister instance = new DecisionRegister();
        instance.addDecision(d);
        ArrayList<String> expResult = new ArrayList<String>();
        expResult.add(d.toString());
        ArrayList<String> result = instance.decisionLstToString();
        assertEquals(expResult, result);
       
    }
    
}
