/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class CongressTest {
    
    public CongressTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getType method, of class Congress.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        Congress instance = new Congress("Event1","Event1","startDate","endDate","Porto");
        String expResult = "Congress";
        String result = instance.getType();
        assertEquals(expResult, result);

    }

    /**
     * Test of toString method, of class Congress.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Congress instance = new Congress("Event1","Event1","startDate","endDate","Porto");
        String expResult = "Event1";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Congress.
     * 
     * when it returns true
     */
    @Test
    public void testEqualsTrue() {
        System.out.println("equalsTrue");
        Congress obj = new Congress("Congress1","Congress1","Date","Date","Porto");
        Congress instance = new Congress("Congress1","Congress1","Date","Date","Porto");
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of equals method, of class Congress.
     * 
     * when it returns false
     */
    @Test
    public void testEqualsFalse() {
        System.out.println("equalsFalse");
        Congress obj = new Congress("Congress1","Congress1","Date","Date","Porto");
        Congress instance = new Congress("Congress2","Congress2","When","Done","Braga");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    /**
     * test of equals method, of class Congress
     * 
     * when its comparing of a different type
     */
     @Test
    public void testEqualsObjectDifferentType() {
        System.out.println("equalsObjectDifferentType");
        FairCenter obj = new FairCenter();
        Congress instance = new Congress("Congress2","Congress2","When","Done","Braga");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of hashCode method, of class Congress
     * 
     * returns the expected number
     */
    @Test
    public void testHashCode() {
        Congress instance = new Congress("Congress1","Congress1","Date","Date","Porto");
        int expResult = 99;
        int result = instance.hashCode();
        assertEquals(expResult,result);
    }

    /**
     * Test of isSameType method, of class Congress.
     * 
     * in false case
     */
    @Test
    public void testIsSameTypeFalse() {
        System.out.println("isSameTypeFalse");
        Object obj = null;
        Congress instance = new Congress("Congress1","Congress1","when","done","braga");
        boolean expResult = false;
        boolean result = instance.isSameType(obj);
        assertEquals(expResult, result);
        
        FairCenter f = new FairCenter();
        result = instance.isSameType(f);
        assertEquals(expResult,result);
    }
   
    
    /**
     * Test of isSameType method, of class Congress
     * 
     * in true case
     */
    @Test 
    public void testIsSameTypeSucess() {
        System.out.println("isSameTypeTrue");
        Congress obj = new Congress("blank","pss","hello","bye","NYC");
        Congress instance = new Congress("Congress1","Congress1","when","done","braga");
        boolean expResult = true;
        boolean result = instance.isSameType(obj);
        assertEquals(expResult,result);
    }
            
}
