/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Manuel Sambade
 */
public class FAEsRegisterTest {

    public FAEsRegisterTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    
    private static FAE f = new FAE("name", "email", "username", "password");

    /**
     * Test of getFaeList method, of class FAEsRegister.
     */
    @Test
    public void testGetFaeList() {
        System.out.println("getAppplicationList");
        FAEsRegister instance = new FAEsRegister();
        ArrayList<FAE> expResult = new ArrayList<FAE>();
        expResult.add(f);
        instance.addFAE(f);
        ArrayList<FAE> result = instance.getFaeList();
        assertEquals(expResult, result);

    }

    /**
     * Test of getFAE method, of class FAEsRegister.
     */
    @Test
    public void testGetFAE() {
        System.out.println("getFAE");
        int index = 0;
        FAEsRegister instance = new FAEsRegister();
        instance.addFAE(f);
        FAE expResult = f;
        FAE result = instance.getFAE(index);
        assertEquals(expResult, result);

    }

    /**
     * Test of addFAE method, of class FAEsRegister.
     */
    @Test
    public void testAddFAE() {
        System.out.println("addFAE");
        ArrayList<FAE> expResult = new ArrayList<FAE>();
        expResult.add(f);
        FAEsRegister instance = new FAEsRegister();
        instance.addFAE(f);
        ArrayList<FAE> result = instance.getFaeList();

        assertEquals(expResult, result);

    }

    /**
     * Test of removeFAE method, of class FAEsRegister.
     */
    @Test
    public void testRemoveFAE() {
        System.out.println("removeFAE");

        FAEsRegister instance = new FAEsRegister();
        instance.addFAE(f);
        assertEquals(f,instance.getFAE(0));
        instance.removeFAE(f);
        assertEquals(0, instance.getSize());
    }

    /**
     * Test of indexOf method, of class FAEsRegister.
     */
    @Test
    public void testIndexOf() {
        System.out.println("indexOf");
        FAEsRegister instance = new FAEsRegister();
        instance.addFAE(f);
        int expResult = 0;
        int result = instance.indexOf(f);
        assertEquals(expResult, result);

    }

    /**
     * Test of size method, of class FAEsRegister.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        ArrayList<FAE> array = new ArrayList<FAE>();
        array.add(f);
        FAEsRegister instance = new FAEsRegister();
        instance.addFAE(f);
        int expResult = array.size();
        int result = instance.getSize();
        assertEquals(expResult, result);

    }
    
    /**
     * Test of getFAEByUsername method, of class FAEsRegister
     */
    @Test
    public void testGetFAEByUsername() {
        System.out.println("GetFAEByUsername");
        FAEsRegister instance = new FAEsRegister();
        instance.addFAE(f);
        assertEquals(1,instance.getSize());
        assertEquals(0,instance.getFAEByUsername(f.getUsername()));
        instance.removeFAE(f);
        assertEquals(0,instance.getSize());
    }

}
