/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Manuel Sambade
 */
public class FairCenterTest {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    private static final FAE F1 = new FAE("João", "joao@yahoo.com", "johnny", "hashedPW");

    /**
     * Test of getEventLog method, of class FairCenter.
     */
    @Test
    public void testGetEventLog() {
        System.out.println("getEventLog");
        FairCenter instance = new FairCenter();
        EventRegister result = instance.getEventRegister();
        assertNotNull(result);
    }

    /**
     * Test of createNewEvent method, of class FairCenter.
     *
     * Creation of a new Congress
     */
    @Test
    public void testCreateNewEventCongress() {
        System.out.println("createNewEventCongress");
        String title = "Event2";
        String description = "2nd event";
        String beginingDate = "03/12/95";
        String endDate = "03/01/96";
        String placeRealization = "Lisbon";
        int type = 1;
        FairCenter instance = new FairCenter();
        int expResult = 0;
        int result = instance.createNewEvent(title, description, beginingDate, endDate, placeRealization, type);
        assertEquals(expResult, result);

    }

    /**
     * Test of createNewEvent method, of class FairCenter
     *
     * Creation of a new Exhibition
     */
    @Test
    public void testCreateNewEventExhibition() {
        System.out.println("createNewEventExhibition");
        String title = "Event2";
        String description = "2nd event";
        String beginingDate = "03/12/95";
        String endDate = "03/01/96";
        String placeRealization = "Lisbon";
        int type = 2;
        FairCenter instance = new FairCenter();
        int expResult = 0;
        int result = instance.createNewEvent(title, description, beginingDate, endDate, placeRealization, type);
        assertEquals(expResult, result);
        Exhibition result2 = new Exhibition(title, description, beginingDate, endDate, placeRealization);
        Exhibition ex = (Exhibition) instance.getEventRegister().getEvent(0);
        assertEquals(ex, result2);
    }

    /**
     * Test of createNewEvent method, of class FairCenter
     *
     * when its doesnt create anything at all, returns a invalid index
     */
    @Test
    public void testCreateNewEventFail() {
        System.out.println("createNewEventFail");
        String title = "Event2";
        String description = "2nd event";
        String beginingDate = "03/12/95";
        String endDate = "03/01/96";
        String placeRealization = "Lisbon";
        int type = -2; //it only can be 1 or 2
        FairCenter instance = new FairCenter();
        int expResult = -1; //valid index can only greater or equal than zero
        int result = instance.createNewEvent(title, description, beginingDate, endDate, placeRealization, type);
        assertEquals(expResult, result);
    }

    /**
     * Test of getUsersRegister method, of class FairCenter.
     */
    @Test
    public void testGetUsersRegister() {
        System.out.println("getUsersRegister");
        FairCenter instance = new FairCenter();
        UserRegister result = instance.getUsersRegister();
        assertNotNull(result);
    }

    /**
     * Test of createNewEvent method, of class FairCenter.
     */
    @Test
    public void testCreateNewEvent() {
        System.out.println("createNewEvent");
        String title = "titulo";
        String description = "descrição";
        String beginingDate = "begining date";
        String endDate = "ending date";
        String placeRealization = "realization place";
        int type = 1;
        FairCenter instance = new FairCenter();
        int expResult = 0;
        int result = instance.createNewEvent(title, description, beginingDate, endDate, placeRealization, type);
        assertEquals(expResult, result);
    }

    /**
     * Test of getGlobalAcceptanceRate method, of class FairCenter.
     */
    @Test
    public void testGetGlobalAcceptanceRate() {
        System.out.println("getGlobalAcceptanceRate");
        FairCenter instance = new FairCenter();
        instance.createNewEvent("Congresso1", "1º Congresso Lapr2", "09/06/2017", "10/06/2017", "DEI", 1);
        instance.createNewEvent("Exposição1", "1º Exposição Lapr2", "09/06/2017", "10/06/2017", "DEI", 2);
        Application app1 = new Application(1, "Descrição1", 200, 200);
        Application app2 = new Application(0, "Descrição2", 300, 200);
        instance.getEventRegister().getEvent(0).getApplicationRegister().addApplication(app1);
        instance.getEventRegister().getEvent(1).getApplicationRegister().addApplication(app2);
        double expResult = 0.5;
        double result = instance.getGlobalAcceptanceRate();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setEventRegister method, of class FairCenter.
     */
    @Test
    public void testSetEventRegister() {
        System.out.println("setEventRegister");
        EventRegister events_register = null;
        FairCenter instance = new FairCenter();
        instance.setEventRegister(events_register);
    }

    /**
     * Test of getEventRegister method, of class FairCenter.
     */
    @Test
    public void testGetEventRegister() {
        System.out.println("getEventRegister");
        FairCenter instance = new FairCenter();
        EventRegister result = instance.getEventRegister();
        assertNotNull(result);
        assertEquals(0, result.getEventList().size());

    }

    /**
     * Test of createNewStand method, of class FairCenter.
     */
    @Test
    public void testCreateNewStand() {
        System.out.println("createNewStand");
        String description = "Stand324";
        int area = 324;
        FairCenter instance = new FairCenter();
        Stand expResult = new Stand(description, area);
        Stand result = instance.createNewStand(description, area);
        assertEquals(expResult, result);
    }

    /**
     * Test of getFAEList method, of class FairCenter.
     */
    @Test
    public void testGetFAEList() {
        System.out.println("getFAEList");
        FairCenter instance = new FairCenter();
        ArrayList<FAE> expResult = new ArrayList<>();
        ArrayList<FAE> result = instance.getFAEList();
        assertEquals(expResult, result);
    }

}
