/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ricardo Pinto
 */
public class ApplicationTest {

    public ApplicationTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    private static final FAE F1 = new FAE("João", "joao@yahoo.com", "johnny", "hashedPW");

//    /**
//     * Test of getCompanyName method, of class Application.
//     */
//    @Test
//    public void testGetCompanyName() {
//        System.out.println("getCompanyName");
//        Application instance = new Application("aaa", "rr", 123456789);
//        String expResult = "aaa";
//        String result = instance.getCompanyName();
//        assertEquals(expResult, result);
//
//    }
//    /**
//     * Test of setCompanyName method, of class Application.
//     */
//    @Test
//    public void testSetCompanyName() {
//        System.out.println("setCompanyName");
//        Application instance = new Application("aaa", "rr", 123456789);
//        String companyName = "bbb";
//        instance.setCompanyName(companyName);
//
//    }
//    /**
//     * Test of getAddress method, of class Application.
//     */
//    @Test
//    public void testGetAddress() {
//        System.out.println("getAddress");
//        Application instance = new Application("aaa", "rr", 123456789);
//        String expResult = "rr";
//        String result = instance.getAddress();
//        assertEquals(expResult, result);
//
//    }
//
//    /**
//     * Test of setAddress method, of class Application.
//     */
//    @Test
//    public void testSetAddress() {
//        System.out.println("setAddress");
//        String address = "ii";
//        Application instance = new Application("aaa", "rr", 123456789);
//        instance.setAddress(address);
//
//    }
//
//    /**
//     * Test of getPhoneNumber method, of class Application.
//     */
//    @Test
//    public void testGetPhoneNumber() {
//        System.out.println("getPhoneNumber");
//        Application instance = new Application("aaa", "rr", 123456789);
//        int expResult = 123456789;
//        int result = instance.getPhoneNumber();
//        assertEquals(expResult, result);
//
//    }
//
//    /**
//     * Test of setPhoneNumber method, of class Application.
//     */
//    @Test
//    public void testSetPhoneNumber() {
//        System.out.println("setPhoneNumber");
//        int phoneNumber = 123456789;
//        Application instance = new Application("aaa", "rr", 999999999);
//        instance.setPhoneNumber(phoneNumber);
//
//    }
//    /**
//     * Test of toString method, of class Application.
//     */
//    @Test
//    public void testToString() {
//        System.out.println("toString");
//        Application instance = new Application("aaa", "rr", 123456789);
//        String expResult = "Application made by aaa";
//        String result = instance.toString();
//        assertEquals(expResult, result);
//
//    }
    /**
     * Test of setDecisionRegister method, of class Application.
     */
    @Test
    public void testSetDecisionRegister() {
        System.out.println("setDecisionRegister");
        DecisionRegister DecisionREG = new DecisionRegister();
        Decision d = new Decision(F1, 3, 3, 3, 3, "nice");
        DecisionREG.addDecision(d);
        Application instance = new Application("Decription", 200, 200);
        instance.setDecisionRegister(DecisionREG);
    }

    /**
     * Test of getDecisionRegister method, of class Application.
     */
    @Test
    public void testGetDecisionRegister() {
        System.out.println("getDecisionRegister");
        Application instance = new Application("Decription", 200, 200);
        DecisionRegister dr = new DecisionRegister();
        instance.setDecisionRegister(dr);
        DecisionRegister expResult = dr;
        DecisionRegister result = instance.getDecisionRegister();
        assertEquals(expResult, result);
    }

//    /**
//     * Test of showInfo method, of class Application.
//     */
    @Test
    public void testShowInfo() {
        System.out.println("showInfo");
        String description = "Hey";
        int invites = 40;
        String state = "accepted";
        Application instance = new Application(1,description,420,40);
        String expResult = description + "\nNumber of Invites : " + invites + "\nApplication was " + state;
        String result = instance.showInfo();
        assertEquals(expResult, result);
        state = "rejected";
        Application instance2 = new Application(0,description,420,40);
        expResult = description + "\nNumber of Invites : " + invites + "\nApplication was " + state;
        result = instance2.showInfo();
        assertEquals(expResult,result);
        Application instance3 = new Application(description,420,40);
        state = "not decided yet";
        expResult = description + "\nNumber of Invites : " + invites + "\nApplication was " + state;
        result = instance3.showInfo();
        assertEquals(expResult,result);
    }
    /**
     * Test of equals method, of class Application.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Application o = new Application("Description", 200, 200);
        Application instance = new Application("Description", 200, 200);
        boolean expResult = true;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);
        FAE f = new FAE("fae1","fae1@centro.pt","fae1@centro.pt","password");
        expResult = false;
        result = instance.equals(f);
        assertEquals(expResult,result);
        Object p = null;
        result = instance.equals(p);
        assertEquals(expResult,result);
    }

    /**
     * Test of getBoothArea method, of class Application.
     */
    @Test
    public void testGetBoothArea() {
        System.out.println("getBoothArea");
        Application instance = new Application("Decription", 200, 200);
        int expResult = 200;
        int result = instance.getBoothArea();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFinalAcceptance method, of class Application.
     */
    @Test
    public void testGetFinalAcceptance() {
        System.out.println("getFinalAcceptance");
        Application instance = new Application(1, "Decription", 200, 200);
        int expResult = 1;
        int result = instance.getFinalAcceptance();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListKeywords method, of class Application.
     */
    @Test
    public void testGetListKeywords() {
        System.out.println("getListKeywords");
        Application instance = new Application("Decription", 200, 200);
        ArrayList<Keyword> result = instance.getListKeywords();
        assertNotNull(result);
        assertEquals(0, result.size());
    }

    /**
     * Test of getDescription method, of class Application.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        Application instance = new Application("descricao",50,20);
        String expResult = "descricao";
        String result = instance.getDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of getInvites method, of class Application.
     */
    @Test
    public void testGetInvites() {
        System.out.println("getInvites");
        Application instance = new Application(1,"descricao",50,40);
        int expResult = 40;
        int result = instance.getInvites();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDescription method, of class Application.
     */
    @Test
    public void testSetDescription() {
        System.out.println("setDescription");
        String new_description = "descricao";
        Application instance = new Application("something",30,40);
        instance.setDescription(new_description);
        assertEquals(new_description,instance.getDescription());
    }

    /**
     * Test of setBoothArea method, of class Application.
     */
    @Test
    public void testSetBoothArea() {
        System.out.println("setBoothArea");
        int new_area = 50;
        Application instance = new Application("descricao",40,20);
        instance.setBoothArea(new_area);
        assertEquals(new_area,instance.getBoothArea(),0);
    }

    /**
     * Test of setInvites method, of class Application.
     */
    @Test
    public void testSetInvites() {
        System.out.println("setInvites");
        int new_invites = 40;
        Application instance = new Application("descricao",54,20);
        instance.setInvites(new_invites);
        assertEquals(new_invites,instance.getInvites());
    }

    /**
     * Test of toString method, of class Application.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Application instance = new Application(1,"description",40,20);
        String expResult = "Application: description";
        String result = instance.toString();
        assertEquals(expResult, result);

    }

    /**
     * Test of hashCode method, of class Application.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Application instance = new Application(1,"descricao",20,20);
        int expResult = 1500107900;
        int result = instance.hashCode();
        System.out.println(result);
        assertEquals(expResult, result);
    }

    /**
     * Test of getInviteQt method, of class Application.
     */
    @Test
    public void testGetInviteQt() {
        System.out.println("getInviteQt");
        Application instance = new Application(1,"descricao",20,20);
        int expResult = 20;
        int result = instance.getInviteQt();
        assertEquals(expResult, result);
    }

    /**
     * Test of getStand method, of class Application.
     */
    @Test
    public void testGetStand() {
        System.out.println("getStand");
        Application instance = new Application(1,"descricao",20,20);
        Stand expResult = new Stand("stand1",30);
        instance.assignStand(expResult);
        Stand result = instance.getStand();
        assertEquals(expResult, result);
    }

    /**
     * Test of assignStand method, of class Application.
     */
    @Test
    public void testAssignStand() {
        System.out.println("assignStand");
        Stand s = new Stand("stand30",30);
        Application instance = new Application(1,"descricao",20,20);
        instance.assignStand(s);
        assertEquals(s,instance.getStand());
        
    }

    /**
     * Test of hasStandAssigned method, of class Application.
     */
    @Test
    public void testHasStandAssigned() {
        System.out.println("hasStandAssigned");
        Stand s = new Stand("stand30",30);
        Application instance = new Application(1,"descricao",20,20);
        boolean expResult = false;
        boolean result = instance.hasStandAssigned();
        assertEquals(expResult, result);
        expResult = true;
        instance.assignStand(s);
        result = instance.hasStandAssigned();
        assertEquals(expResult,result);
    }

}
