/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Manuel Sambade
 */
public class DecisionTest {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

   private static final FAE F1 = new FAE("João", "joao@yahoo.com", "johnny", "hashedPW");
   
   private static final String justification = "Because i have permission";
   private static Decision D1 = new Decision(F1, 1,1,1,1,justification);
    

   

    /**
     * Test of getJustification method, of class Decision.
     */
    @Test
    public void testGetJustification() {
        System.out.println("getJustification");
        String expResult = justification;
        String result = D1.getJustification();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of setJustification method, of class Decision.
     */
    @Test
    public void testSetJustification() {
        System.out.println("setJustification");
        D1.setJustification(justification);
        assertEquals(justification,D1.getJustification());
    }

    /**
     * Test of getDecisionFAE method, of class Decision.
     */
    @Test
    public void testGetDecisionFAE() {
        System.out.println("getDecisionFAE");
        FAE expResult = F1;
        FAE result = D1.getDecisionFAE();
        assertEquals(expResult, result);

    }

    /**
     * Test of getFinalMean method, of class Decision.
     */
    @Test
    public void testGetFinalMean() {
        System.out.println("getFinalMean");
        Decision instance = new Decision(F1,2,3,2,4,"Because i have permission");
        double expResult = 2.75;
        double result = instance.getFinalMean();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setFaeKnowledge method, of class Decision.
     */
    @Test
    public void testSetFaeKnowledge() {
        System.out.println("setFaeKnowledge");
        int faeKnowledge = 0;
        Decision instance = D1;
        instance.setFaeKnowledge(faeKnowledge);
    }

    /**
     * Test of setAppAdequacy method, of class Decision.
     */
    @Test
    public void testSetAppAdequacy() {
        System.out.println("setAppAdequacy");
        int appAdequacy = 0;
        Decision instance = D1;
        instance.setAppAdequacy(appAdequacy);
    }

    /**
     * Test of setInvitations method, of class Decision.
     */
    @Test
    public void testSetInvitations() {
        System.out.println("setInvitations");
        int invitations = 0;
        Decision instance = D1;
        instance.setInvitations(invitations);
    }

    /**
     * Test of setOverall method, of class Decision.
     */
    @Test
    public void testSetOverall() {
        System.out.println("setOverall");
        int overall = 0;
        Decision instance = D1;
        instance.setOverall(overall);
    }

    /**
     * Test of calculateFinalMean method, of class Decision.
     */
    @Test
    public void testCalculateFinalMean() {
        System.out.println("calculateFinalMean");
        Decision instance = new Decision(F1,2,3,2,4,"Because i have permission");
        double expResult = 2.75;
        double result = instance.calculateFinalMean();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of toString method, of class Decision.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Decision instance = new Decision(F1,2,3,2,4,"Because i have permission");
        String expResult = "FAE: João" + " Mean: 2.75 with justification: Because i have permission";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of setFinalMean method, of class Decision.
     */
    @Test
    public void testSetFinalMean() {
        System.out.println("setFinalMean");
        double finalMean = 3.75;
        Decision instance = D1;
        instance.setFinalMean(finalMean);
    }
 }
