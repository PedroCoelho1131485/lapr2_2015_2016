/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class OrganizersRegisterTest {
    
    public OrganizersRegisterTest() {
    }
    
    private OrganizersRegister organizer_list;
    private Organizer instance;
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        this.organizer_list = new OrganizersRegister();
        this.instance = new Organizer("Peter","someone@gmail.com","pete","123");
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addOrganizer method, of class OrganizersRegister.
     */
    @Test
    public void testAddOrganizer() {
        System.out.println("addOrganizer");
        this.organizer_list.addOrganizer(instance);
        boolean result = true;
        assertEquals(result,this.organizer_list.getOrganizersList().contains(instance));
    }

    /**
     * Test of getPosition method, of class OrganizersRegister.
     */
    @Test
    public void testGetPosition() {
        System.out.println("getPosition");
        int expResult = 0;
        this.organizer_list.addOrganizer(instance);
        int result = this.organizer_list.getPosition(this.instance);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeOrganizer method, of class OrganizersRegister.
     */
    @Test
    public void testRemoveOrganizer() {
        System.out.println("removeOrganizer");
        this.organizer_list.addOrganizer(instance);
        this.organizer_list.removeOrganizer(this.instance);
        boolean result = false;
        assertEquals(result,this.organizer_list.getOrganizersList().contains(this.instance));
    }


    /**
     * Test of getOrganizersList method, of class OrganizersRegister.
     */
    @Test
    public void testGetOrganizersList() {
        System.out.println("getOrganizersList");
        ArrayList<Organizer> result = this.organizer_list.getOrganizersList();
        this.organizer_list.addOrganizer(this.instance);
        assertEquals(this.organizer_list.getOrganizersList(),result);
    }
    
}
