/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Manuel Sambade
 */
public class OrganizerTest {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of getRole method, of class Organizer.
     */
    @Test
    public void testGetRole() {
        System.out.println("getRole");
        Organizer instance = new Organizer("asgagwe","agafrq","adgawrfgw","151325");
        String expResult = UserRole.ORGANIZER;
        String result = instance.getRole();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of the method toString, of class Organizer
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Organizer o1 = new Organizer("organizer","organizer@centro.pt","organizer@centro.pt","password");
        String expResult = "organizer with email organizer@centro.pt";
        String result = o1.toString();
        assertEquals(expResult,result);

    }

    /**
     * Test of getState method, of class Organizer.
     */
    @Test
    public void testGetState() {
        System.out.println("getState");
        Organizer instance = new Organizer("hey","hey","hey","hey");
        boolean expResult = false;
        boolean result = instance.getState();
        assertEquals(expResult, result);
        instance.changeState();
        expResult = true;
        result = instance.getState();
        assertEquals(expResult,result);
    }

    /**
     * Test of changeState method, of class Organizer.
     */
    @Test
    public void testChangeState() {
        System.out.println("changeState");
        Organizer instance = new Organizer("good","good","good","nice");
        instance.changeState();
        assertEquals(true,instance.getState());
    }

}
