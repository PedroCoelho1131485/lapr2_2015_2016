/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class ExhibitionTest {
    
    public ExhibitionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getType method, of class Exhibition.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        Exhibition instance = new Exhibition("Event1","Event1","startDate","endDate","Porto");
        String expResult = "Exhibition";
        String result = instance.getType();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Exhibition.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Exhibition instance = new Exhibition("Event1","Event1","startDate","endDate","Porto");
        String expResult = "Event1";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Exhibition.
     * 
     * in the case that returns true
     */
    @Test
    public void testEqualsTrue() {
        System.out.println("equalsTrue");
        Exhibition obj = new Exhibition("Exhbition1","Exhibition1","Date","Date","Porto");
        Exhibition instance = new Exhibition("Exhbition1","Exhibition1","Date","Date","Porto");
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of equals method, of class Exhibition.
     * 
     * in the case that returns true
     */
    @Test
    public void testEqualsFalse() {
        System.out.println("equalsFalse");
        Exhibition obj = new Exhibition("Exhbition1","Exhibition1","Date","Date","Porto");
        Exhibition instance = new Exhibition("Exhbition2","Exhibition2","when","Done","Evora");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class Exhibition.
     * 
     *  in case that both objects are a different type or classe
     */
    @Test
    public void testEqualsDifferentType() {
        System.out.println("equalsDifferentType");
        FairCenter obj = new FairCenter();
        Exhibition instance = new Exhibition("Exhbition2","Exhibition2","when","Done","Evora");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class Exhibition.
     *
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Exhibition instance = new Exhibition("Exhbition1","Exhibition1","Date","Date","Porto");
        int expResult = 98;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isSameType method, of class Exhibition
     * 
     * in false case
     */
    @Test
    public void testIsSameTypeFalse() {
        System.out.println("isSameTypeFalse");
        Object obj = null;
        Exhibition instance = new Exhibition("Congress1","Congress1","when","done","braga");
        boolean expResult = false;
        boolean result = instance.isSameType(obj);
        assertEquals(expResult, result);
        
        FairCenter f = new FairCenter();
        result = instance.isSameType(f);
        assertEquals(expResult,result);
    }
   
    
    /**
     * Test of isSameType method, of class Exhibition
     * 
     * in true case
     */
    @Test 
    public void testIsSameTypeSucess() {
        System.out.println("isSameTypeTrue");
        Exhibition obj = new Exhibition("blank","pss","hello","bye","NYC");
        Exhibition instance = new Exhibition("Congress1","Congress1","when","done","braga");
        boolean expResult = true;
        boolean result = instance.isSameType(obj);
        assertEquals(expResult,result);
    }
    
}
