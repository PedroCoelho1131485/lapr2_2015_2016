/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import static lapr.project.model.User.encrypt;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Manuel Sambade
 */
public class ParticipantTest {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of getName method, of class Participant.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Participant instance = new Participant("Peter","peter_shmikel@sapo.pt","petershk","123");
        String expResult = "Peter";
        String result = instance.getName();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setName method, of class Participant.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "Peter";
        Participant instance = new Participant(name,"peter_shmikel@sapo.pt","petershk","123");
        instance.setName(name);
        assertEquals(name,instance.getName());
    }

    /**
     * Test of getEmail method, of class Participant.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        Participant instance = new Participant("Peter","peter_shmikel@sapo.pt","petershk","123");
        String expResult = "peter_shmikel@sapo.pt";
        instance.setEmail(expResult);
        String result = instance.getEmail();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setEmail method, of class Participant.
     */
    @Test
    public void testSetEmail() {
        System.out.println("setEmail");
        String email = "something@sapo.pt";
        Participant instance = new Participant("Peter",email,"petershk","123");
        instance.setEmail(email);
        assertEquals(email,instance.getEmail());
    }

    /**
     * Test of getUsername method, of class Participant.
     */
    @Test
    public void testGetUsername() {
        System.out.println("getUsername");
        Participant instance = new Participant("Peter","hello@gmail.com","petershk","123");
        String expResult = "petershk";
        instance.setUsername(expResult);
        String result = instance.getUsername();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setUsername method, of class Participant.
     */
    @Test
    public void testSetUsername() {
        System.out.println("setUsername");
        String username = "petes";
        Participant instance = new Participant("Peter","hello@gmail.com",username,"123");
        instance.setUsername(username);
        assertEquals(username,instance.getUsername());
    }

    /**
     * Test of getPassword method, of class Participant.
     */
    @Test
    public void testGetHashedPassword() {
        System.out.println("getHashedPassword");
        Participant instance = new Participant("Peter","hello@gmail.com","petershk","123");
        String expResult = "123";
        instance.setPassword(expResult);
        String result = instance.getHashedPassword();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setPassword method, of class Participant.
     */
    @Test
    public void testSetHashedPassword() {
        System.out.println("setHashedPassword");
        String hashedPassword = "123";
        Participant instance = new Participant("Peter","hello@gmail.com","petershk",hashedPassword);
        instance.setPassword(hashedPassword);
        assertEquals(hashedPassword,instance.getHashedPassword());
    }

    /**
     * Test of toString method, of class Participant.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Participant instance = new Participant("Peter","hello@gmail.com","petershk","123");
        String name = "Peter";
        String email = "hello@gmail.com";
        String username = "petershk";
        String pass = "123";
        String password = encrypt(pass.toCharArray());
        String expResult = "Name: " + name + ", email: " + email + ", userName: " 
                + username + ", password: " + password;
        String result = instance.toString();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getRole method, of class Participant.
     */
    @Test
    public void testGetRole() {
        System.out.println("getRole");
        Participant instance = new Participant("peppa","parat","afaqfr","1agad");
        String expResult = UserRole.PARTICIPANT;
        String result = instance.getRole();
        assertEquals(expResult, result);
    }
    
}
