/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class EventTest {

    public EventTest() {
    }

    private Event event1;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    private static final FAE F1 = new FAE("João", "joao@yahoo.com", "johnny", "hashedPW");

    @Before
    public void setUp() {

        String title = "Motorcycle Lisbon Event 2017";
        String description = "A gathering of classic motorcycles in Lisbon for the first time and an epic concert to end day in the most awesome way ever.";
        String beginingDate = "01-07-2017";
        String endDate = "02-07-2017";
        String placeRealization = "Lisbon";
        event1 = new Event(title, description, beginingDate, endDate, placeRealization);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test for the sucess of the creation of a new object of Type Event
     */
    @Test
    public void testConstructorEventSuccesufull() {

        //not using real data for this variables 
        String title = "Motorcycle Lisbon Event 2017";
        String description = "A gathering of classic motorcycles in Lisbon for the first time and an epic concert to end day in the most awesome way ever.";
        String beginingDate = "01-07-2017";
        String endDate = "02-07-2017";
        String placeRealization = "Lisbon";

        Event motorEvent = new Event(title, description, beginingDate, endDate, placeRealization);

        assertNotNull(motorEvent);
    }

    /**
     * method of unit test of getTitle
     */
    @Test
    public void testGetTitle() {

        String expected = "Motorcycle Lisbon Event 2017";
        String result = event1.getTitle();

        assertEquals(expected, result);
    }

    /**
     * method of unit test for the get description
     */
    @Test
    public void testGetDescription() {

        String description = "A gathering of classic motorcycles in Lisbon for the first time and an epic concert to end day in the most awesome way ever.";

        String result = event1.getDescription();

        assertEquals(description, result);
    }

    /**
     * method of unit test of get place realization
     */
    @Test
    public void testGetPlaceRealization() {

        String place = "Lisbon";

        String result = event1.getPlaceRealization();

        assertEquals(place, result);
    }

    /**
     * Test of getApllicationRegister method, of class Event.
     */
    @Test
    public void testGetApllicationRegister() {
        System.out.println("getApllicationRegister");
        ApplicationRegister result = event1.getApplicationRegister();
        assertNotNull(result);
        assertEquals(0, event1.getApplicationRegister().size());
    }

    /**
     * Test of getFAEsRegister method, of class Event.
     */
    @Test
    public void testGetFAEsRegister() {
        System.out.println("getFAEsRegister");
        FAEsRegister result = event1.getFAEsRegister();
        assertNotNull(result);
    }

    /**
     * Test of toString method, of class Event.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = event1.getTitle();
        String result = event1.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of setOrganizer method, of class Event.
     *
     * When it adds sucessfully this Organizer
     */
    @Test
    public void testSetOrganizerSucessufully() {
        System.out.println("setOrganizerSucessufully");
        Organizer o = new Organizer("Peter", "petermike@gmail.com", "peke", "312515");
        boolean expResult = true;
        boolean result = event1.setOrganizer(o);
        assertEquals(expResult, result);
    }

    /**
     * Test of setOrganizer method, of class Event
     *
     * When it cant be added on the list, for already exist
     */
    @Test
    public void testSetOrganizerFail() {
        System.out.println("setOrganizerFail");
        Organizer o = new Organizer("Peter", "petermike@gmail.com", "peke", "312515");
        boolean expResult = false;
        event1.getOrganizersRegister().addOrganizer(o);
        boolean result = event1.setOrganizer(o);
        assertEquals(expResult, result);
    }

    /**
     * Test of the getOrganizersRegister method, of class Event
     */
    @Test
    public void testGetOrganizersRegister() {
        System.out.println("getOrganizersRegister");
        assertNotNull(event1.getOrganizersRegister());
    }

    /**
     * Test of getBeginingDate method, of class Event.
     */
    @Test
    public void testGetBeginingDate() {
        System.out.println("getBeginingDate");
        Event instance = new Event("Evento1", "descrição", "01/01/1998", "27/11/1998", "porto");
        String expResult = "01/01/1998";
        String result = instance.getBeginingDate();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEndDate method, of class Event.
     */
    @Test
    public void testGetEndDate() {
        System.out.println("getEndDate");
        Event instance = new Event("Evento1", "descrição", "01/01/1998", "27/11/1998", "porto");
        String expResult = "27/11/1998";
        String result = instance.getEndDate();
        assertEquals(expResult, result);
    }

    /**
     * Test of setOrganizer method, of class Event.
     */
    @Test
    public void testSetOrganizer() {
        System.out.println("setOrganizer");
        Organizer o = new Organizer("name", "email", "UserName", "hashedPassword");
        Event instance = new Event("Evento1", "descrição", "01/01/1998", "27/11/1998", "porto");;
        boolean expResult = true;
        boolean result = instance.setOrganizer(o);
        assertEquals(expResult, result);
    }

    /**
     * Test of getApplicationRegister method, of class Event.
     */
    @Test
    public void testGetApplicationRegister() {
        System.out.println("getApplicationRegister");
        ApplicationRegister result = event1.getApplicationRegister();
        assertNotNull(result);
    }

    /**
     * Test of getAcceptanceRate method, of class Event.
     */
    @Test
    public void testGetAcceptanceRate() {
        System.out.println("getAcceptanceRate");
        Event instance = new Event("Evento1", "descrição", "01/01/1998", "27/11/1998", "porto");
        Application a3 = new Application(1, "Description3", 200, 200);
        Application a4 = new Application(0, "Description4", 300, 300);
        ApplicationRegister apR = new ApplicationRegister();
        EventRegister evr = new EventRegister();
        instance.getApplicationRegister().addApplication(a3);
        instance.getApplicationRegister().addApplication(a4);
        double expResult = 0.5;
        double result = instance.getAcceptanceRate();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setApplicationRegister method, of class Event.
     */
    @Test
    public void testSetApplicationRegister() {
        System.out.println("setApplicationRegister");
        ApplicationRegister applicationRegister = null;
        Event instance = new Event("Evento1", "descrição", "01/01/1998", "27/11/1998", "porto");
        instance.setApplicationRegister(applicationRegister);
    }

    /**
     * Test of getStandsRegister method, of class Event.
     */
    @Test
    public void testGetStandsRegister() {
        System.out.println("getStandsRegister");
        StandsRegister result = event1.getStandsRegister();
        assertNotNull(result);
        assertEquals(0, result.getStandsList().size());
    }

    /**
     * Test of setTitle method, of class Event.
     */
    @Test
    public void testSetTitle() {
        System.out.println("setTitle");
        String title = "exposicao2";
        event1.setTitle(title);
        assertEquals(title,event1.getTitle());
    }

    /**
     * Test of setDescription method, of class Event.
     */
    @Test
    public void testSetDescription() {
        System.out.println("setDescription");
        String description = "descricaoDasBoas";
        event1.setDescription(description);
        assertEquals(description,event1.getDescription());
    }

    /**
     * Test of setBeginingDate method, of class Event.
     */
    @Test
    public void testSetBeginingDate() {
        System.out.println("setBeginingDate");
        String beginingDate = "data";
        event1.setBeginingDate(beginingDate);
        assertEquals(beginingDate,event1.getBeginingDate());
    }

    /**
     * Test of setEndDate method, of class Event.
     */
    @Test
    public void testSetEndDate() {
        System.out.println("setEndDate");
        String endDate = "data";
        event1.setEndDate(endDate);
        assertEquals(endDate,event1.getEndDate());
    }

    /**
     * Test of setPlaceRealization method, of class Event.
     */
    @Test
    public void testSetPlaceRealization() {
        System.out.println("setPlaceRealization");
        String placeRealization = "Porto Alegre";
        event1.setPlaceRealization(placeRealization);
        assertEquals(placeRealization,event1.getPlaceRealization());
    }

}
