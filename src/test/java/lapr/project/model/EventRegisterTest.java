/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class EventRegisterTest {

    public EventRegisterTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getEventList method, of class EventLog.
     */
    @Test
    public void testGetEventList() {
        System.out.println("getEventList");
        EventRegister instance = new EventRegister();
        ArrayList<Event> result = instance.getEventList();
        assertNotNull(result);
    }

    /**
     * Test of addEvent method, of class EventLog.
     */
    @Test
    public void testAddEvent() {
        System.out.println("addEvent");
        Event e = new Congress("Porto on Eletronica", "Eletro is awesome", "03-12-1222", "03-12-1233", "Leiria");
        EventRegister instance = new EventRegister();
        instance.addEvent(e);
        assertEquals(true, instance.searchEvent(e));
    }

    /**
     * Test of getPosition method, of class EventLog.
     */
    @Test
    public void testGetPosition() {
        System.out.println("getPosition");
        Event x = new Event("Hello", "Its me", "19/3/1952", "03-99-12", "Porto");
        EventRegister instance = new EventRegister();
        instance.addEvent(x);
        int expResult = 0;
        int result = instance.getPosition(x);
        assertEquals(expResult, result);
    }

    /**
     * Test of searchEvent method, of class EventLog.
     */
    @Test
    public void testSearchEvent() {
        System.out.println("searchEvent");
        Event x = new Event("Event3", "Some Event", "11/11/11", "9/11/2012", "Aveiro");
        EventRegister instance = new EventRegister();
        instance.addEvent(x);
        boolean expResult = true;
        boolean result = instance.searchEvent(x);
        assertEquals(expResult, result);
    }

    /**
     * Test of getEvent method, of class EventRegister.
     */
    @Test
    public void testGetEvent() {
        System.out.println("getEvent");
        int index = 0;
        Event e = new Event("Event3", "Some Event", "11/11/11", "9/11/2012", "Aveiro");
        EventRegister instance = new EventRegister();
        instance.addEvent(e);
        Event expResult = e;
        Event result = instance.getEvent(index);
        assertEquals(expResult, result);
    }

    /**
     * Test of getGlobalMeanRate method, of class EventRegister.
     */
    @Test
    public void testGetGlobalMeanRate() {
        System.out.println("getGlobalMeanRate");
        FAE f = new FAE("name", "email", "username", "password");
        FAE f1 = new FAE("name23", "email32", "usernam32e", "pass42word");
        Decision d = new Decision(f, 2, 3, 3, 3, "ola"); //2.75
        Decision d1 = new Decision(f1, 4, 3, 3, 3, "ola"); //3.25
        Decision d2 = new Decision(f, 1, 3, 3, 3, "ola"); //2.5
        Decision d3 = new Decision(f, 5, 3, 3, 3, "ola");//3.5
        Decision d4 = new Decision(f, 2, 3, 4, 3, "ola");//3
        Decision d5 = new Decision(f1, 3, 3, 3, 3, "ola");//3
        Decision d6 = new Decision(f1, 2, 2, 2, 2, "ola");//2
        Decision d7 = new Decision(f, 2, 5, 5, 5, "ola");//4.25
        Decision d8 = new Decision(f, 2, 3, 4, 5, "ola");//3.5
        Decision d9 = new Decision(f1, 2, 3, 5, 3, "ola");//3.25
        Decision d0 = new Decision(f, 2, 4, 3, 3, "ola");//3
        Application app = new Application("descrição", 123, 12345);
        Application app1 = new Application("descrição1", 132, 45645);
        Application app2 = new Application("descrição2", 653, 19875);
        app.getDecisionRegister().addDecision(d0);
        app1.getDecisionRegister().addDecision(d1);
        app.getDecisionRegister().addDecision(d2);
        app2.getDecisionRegister().addDecision(d3);
        app.getDecisionRegister().addDecision(d4);
        app.getDecisionRegister().addDecision(d5);
        app.getDecisionRegister().addDecision(d6);
        app1.getDecisionRegister().addDecision(d7);
        app.getDecisionRegister().addDecision(d8);
        app1.getDecisionRegister().addDecision(d9);
        app.getDecisionRegister().addDecision(d);
        EventRegister instance = new EventRegister();
        Event e = new Event( "title",  "description",  "beginingDate","endDate","placeRealization");
        e.getApplicationRegister().addApplication(app);
        e.getApplicationRegister().addApplication(app1);
        e.getApplicationRegister().addApplication(app2);
        instance.addEvent(e);
        double expResult = 3.09;
        double result = instance.getGlobalMeanRate();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getGlobalMeanRate method, of class EventRegister.
     */
    @Test
    public void testGetGlobalMeanRateWith0() {
        System.out.println("getGlobalMeanRate");
        EventRegister instance = new EventRegister();
        double expResult = 0.0;
        double result = instance.getGlobalMeanRate();
        assertEquals(expResult, result, 0.0);
    }
}
