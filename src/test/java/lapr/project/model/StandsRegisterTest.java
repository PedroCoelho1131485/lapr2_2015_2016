/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class StandsRegisterTest {
    
    private StandsRegister instance;
    
    public StandsRegisterTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        instance = new StandsRegister();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getStandsList method, of class StandsRegister.
     */
    @Test
    public void testGetStandsList() {
        System.out.println("getStandsList");
        ArrayList<Stand> result = instance.getStandsList();
        assertNotNull(result);
    }

    /**
     * Test of addStand method, of class StandsRegister.
     */
    @Test
    public void testAddStand() {
        System.out.println("addStand");
        Stand stand = new Stand();
        instance.addStand(stand);
        Stand result = instance.getStandsList().get(0);
        assertEquals(stand,result);
        instance.addStand(stand);
        assertEquals(1,instance.getStandsList().size());
    }

    /**
     * Test of removeStand method, of class StandsRegister.
     */
    @Test
    public void testRemoveStand() {
        System.out.println("removeStand");
        Stand stand = new Stand();
        instance.addStand(stand);
        assertEquals(1,instance.getStandsList().size());
        instance.removeStand(stand);
        assertEquals(0,instance.getStandsList().size());

    }

    /**
     * Test of getStand method, of class StandsRegister.
     */
    @Test
    public void testGetStand() {
        System.out.println("getStand");
        int index = 0;
        Stand expResult = new Stand();
        instance.addStand(expResult);
        Stand result = instance.getStand(index);
        assertEquals(expResult, result);
    }

    
    /**
     * Calculate standart Deviation
     * Step 1: Find the mean.
        Step 2: For each data point, find the square of its distance to the mean.
        Step 3: Sum the values from Step 2.
        Step 4: Divide by the number of data points.
        Step 5: Take the square root.
     */
    @Test
    public void testCalculateStandardDeviation() {
        System.out.println("calculateStandardDeviation");
        Stand s = new Stand("stand",400);
        instance.addStand(s);
        // 0  = s with area 400
        // totalArea = 400;
        Stand s1 = new Stand("stand1",600);
        instance.addStand(s1);
        //totalArea = 1000;
        Stand s2 = new Stand("stand2",150);
        instance.addStand(s2);
        // total Area = 1150;
        // mean 1150 / 3 =  383,333333
        // sum = (383,3333 - 400)^2 + (383,3333 - 600)^2 + (383,3333 - 150)^2 
        // standardDeviation = SquareRoot(sum/3) = 
        double expResult = 184.0894;
        double result = Math.round(instance.calculateStandardDeviation() * 10000.0) / 10000.0;
        assertEquals(expResult,result,0.0);
    }
    
     /**
     * Test of calculateFrequencyOfStand method, of class StandsRegister.
     */
    @Test
    public void testCalculateMeanDeviation() {
        System.out.println("calculateMeanDeviation");
        Stand s = new Stand("stand",400);
        instance.addStand(s);
        // 0  = s with area 400
        // totalArea = 400;
        Stand s1 = new Stand("stand1",600);
        instance.addStand(s1);
        //totalArea = 1000;
        Stand s2 = new Stand("stand2",150);
        instance.addStand(s2);
        
        //Find the mean
        //Find the distance of each value from that mean = sum
        // Find the mean of those distances
        // total Area = 1150;
        // current mean = 1150 / 3 = 383,3333
        // sum ABS(383,3333 - 400) + ABS(383,3333 - 6000) + ABS(383,3333 - 150) = 466,66667
        // sum / 3 = 155,5556
        double expResult = 155.5556;
        double result = Math.round(instance.calculateMeanDeviation() * 10000.0) / 10000.0;
        assertEquals(expResult, result,0.0);

    }

    /**
     * Test of calculateMean method, of class StandsRegister.
     */
    @Test
    public void testCalculateMean() {
        System.out.println("calculateMean");
        Stand s = new Stand("stand",400);
        instance.addStand(s);
        // 0  = s with area 400
        // totalArea = 400;
        Stand s1 = new Stand("stand1",600);
        instance.addStand(s1);
        //totalArea = 1000;
        Stand s2 = new Stand("stand2",150);
        instance.addStand(s2);
        // mean = 400 + 600 + 150 / 3
        double expResult = 383.3;
        double result = Math.round(instance.calculateMean() * 10.0) / 10.0;
        assertEquals(expResult, result, 0.0);

    }
    
       /**
     * Test of calculateAreaFrenquecies method, of class StandsRegister.
     */
//    @Test
//    public void testCalculateAreaFrenquecies() {
//        System.out.println("calculateAreaFrenquecies");
//        Stand s1 = new Stand("stand1",55);
//        Stand s2 = new Stand("stand2",50);
//        double[][] expResult = new double[2][5];
//        instance.addStand(s1);
//        instance.addStand(s2);
//        //areas
//        //30
//        //40 
//        //Sturges rule = 1 + 3.322 * log10(2) = (k) 2.00 = classes
//        //range = (55 - 50) / 2) = 2.5
//        //limites : 50 - 52.5 ; 52.5 - 55
//        expResult [0][0] = 50.0;
//        expResult [0][1] = 52.5;
//        expResult [1][0] = 52.5;
//        expResult [1][1] = 55.0;
//        expResult [0][2] = 1;
//        expResult [1][2] = 1;
//        expResult [0][3] = 0.5;
//        expResult [1][3] = 0.5;
//        expResult [0][4] = 0.5;
//        expResult [1][4] = 1.0;
//        double[][] result = instance.calculateAreaFrenquecies();
//        for(int l=0; l < result.length;l++) {
//            for(int c=0; c < 5; c++) {
//        
//        System.out.println(result[l][c]);
//      
//            }
//        }
//          assertArrayEquals(expResult, result);
//    }
}
