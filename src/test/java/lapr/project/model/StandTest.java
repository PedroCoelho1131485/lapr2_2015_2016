/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class StandTest {

    private Stand instance;

    public StandTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        instance = new Stand("Description", 400);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of hashCode method, of class Stand.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        int expResult = -56677412;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Stand.
     *
     * in case of returning true
     */
    @Test
    public void testEqualsTrue() {
        System.out.println("equals");
        Object obj = instance;
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Stand
     *
     * in case of returning false
     */
    @Test
    public void testEqualsFalse() {
        System.out.println("equals");
        FairCenter obj = new FairCenter();
        boolean expResult = false;
        boolean result = instance.equals(null);
        assertEquals(expResult, result);
        result = instance.equals(obj);
        assertEquals(expResult, result);
        Stand obj2 = new Stand();
        result = instance.equals(obj2);
        assertEquals(expResult, result);

    }

    /**
     * Test of getDescription method, of class Stand.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        Stand stand = new Stand("description not very creative", 300);
        String expResult = "description not very creative";
        String result = stand.getDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of getInformation method, of class Stand.
     */
    @Test
    public void testGetInformation() {
        System.out.println("getInformation");
        Stand stand = new Stand("description not very creative", 645);
        String description = "description not very creative";
        int area = 645;
        String expResult = "Stand Info!Description : " + description + "\nwith " + Integer.toString(area) + " of area.";
        String result = stand.getInformation();
        assertEquals(expResult, result);

    }

    /**
     * Test of toString method, of class Stand.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Stand stand = new Stand("10/10", 500);
        String expResult = "10/10";
        String result = stand.toString();
        assertEquals(expResult, result);

    }

    /**
     * Test of getArea method, of class Stand.
     */
    @Test
    public void testGetArea() {
        System.out.println("getArea");
        int expResult = 400;
        int result = instance.getArea();
        assertEquals(expResult, result);
    }


    /**
     * Test of setDescription method, of class Stand.
     */
    @Test
    public void testSetDescription() {
        System.out.println("setDescription");
        String description = "";
        Stand instance = new Stand();
        instance.setDescription(description);
    }


    /**
     * Test of equals method, of class Stand.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        Stand instance = new Stand();
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of isStandAssigned method, of class Stand.
     */
    @Test
    public void testIsStandAssigned() {
        System.out.println("isStandAssigned");
        Stand instance = new Stand("stand1",20);
        boolean expResult = false;
        boolean result = instance.isStandAssigned();
        assertEquals(expResult, result);
        expResult = true;
        instance.changeStandToAssigned();
        result = instance.isStandAssigned();
        assertEquals(expResult,result);
    }

    /**
     * Test of changeStandToAssigned method, of class Stand.
     */
    @Test
    public void testChangeStandToAssigned() {
        System.out.println("changeStandToAssigned");
        Stand instance = new Stand("stand1",100);
        instance.changeStandToAssigned();
        assertEquals(true,instance.isStandAssigned());
    }

}
