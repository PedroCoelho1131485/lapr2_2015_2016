package lapr.project.model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class KeywordTest {
    
    public KeywordTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getValue method, of class Keyword.
     */
    @Test
    public void testGetValue() {
        System.out.println("getValue");
        Keyword instance = new Keyword("Awesome");
        String expResult = "Awesome";
        String result = instance.getValue();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Keyword.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Keyword instance = new Keyword("value");
        String expResult = "value";
        String result = instance.toString();
        assertEquals(expResult, result);

    }

//    /**
//     * Test of equals method, of class Keyword.
//     */
//    @Test
//    public void testEquals() {
//        System.out.println("equals");
//        Object o = null;
//        Keyword instance = null;
//        boolean expResult = false;
//        boolean result = instance.equals(o);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

//    /**
//     * Test of hashCode method, of class Keyword.
//     */
//    @Test
//    public void testHashCode() {
//        System.out.println("hashCode");
//        Keyword instance = null;
//        int expResult = 0;
//        int result = instance.hashCode();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    
}
