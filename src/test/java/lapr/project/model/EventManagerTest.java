/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class EventManagerTest {
    
    private EventManager instance;
    
    public EventManagerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
       instance = new EventManager("Manager","boss@callme.later","username","13");
    }
    
    @After
    public void tearDown() {
    }

    /**
     * test of the Method getRole, of class EventManager
     */
    @Test
    public void testGetRole() {
        System.out.println("getRole");
        String expResult = UserRole.EVENTMANAGER;
        String result = instance.getRole();
        assertEquals(expResult,result);
    }
}
