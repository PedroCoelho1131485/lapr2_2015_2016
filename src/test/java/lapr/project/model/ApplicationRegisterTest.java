/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ricardo Pinto
 */
public class ApplicationRegisterTest {

    public ApplicationRegisterTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    
    private final Application a = new Application("Descrição", 500, 300);

    /**
     * Test of getAppplicationList method, of class ApplicationRegister.
     */
    @Test
    public void testGetAppplicationList() {
        System.out.println("getAppplicationList");
        ApplicationRegister instance = new ApplicationRegister();
        ArrayList<Application> expResult = new ArrayList<Application>();
        expResult.add(a);
        instance.addApplication(a);
        ArrayList<Application> result = instance.getApplicationList();
        assertEquals(expResult, result);

    }

    /**
     * Test of size method, of class ApplicationRegister.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        ApplicationRegister instance = new ApplicationRegister();
        int expResult = 0;
        int result = instance.size();
        assertEquals(expResult, result);

    }

    /**
     * Test of addFAE method, of class ApplicationRegister.
     */
    @Test
    public void testAddApplication() {
        System.out.println("addApplication");
        Application app = new Application(1,"descricao",100,45);
        ApplicationRegister instance = new ApplicationRegister();
        instance.addApplication(app);
        assertEquals(app,instance.getApplication(0));
        instance.addApplication(app);
        assertEquals(1,instance.getApplicationList().size());
    }

    /**
     * Test of indexOf method, of class ApplicationRegister.
     */
    @Test
    public void testIndexOf() {
        System.out.println("indexOf");
        ApplicationRegister instance = new ApplicationRegister();
        instance.addApplication(a);
        int expResult = 0;
        int result = instance.indexOf(a);
        assertEquals(expResult, result);

    }

    /**
     * Test of getApplication method, of class ApplicationRegister.
     */
    @Test
    public void testGetApplication() {
        System.out.println("getApplication");
        int index = 0;
        Application expResult = a;
        ApplicationRegister array = new ApplicationRegister();
        array.addApplication(a);
        Application result = array.getApplication(index);
        assertEquals(expResult, result);

    }

    /**
     * Test of removeApplication method, of class ApplicationRegister.
     */
    @Test
    public void testRemoveApplication() {
        System.out.println("removeApplication");
        Application a = new Application(1,"descricao",30,40);
        ApplicationRegister instance = new ApplicationRegister();
        instance.addApplication(a);
        assertEquals(a,instance.getApplication(0));
        instance.removeApplication(a);
        assertEquals(0,instance.getApplicationList().size());
    }

    /**
     * Test of hasApplication method, of class ApplicationRegister.
     */
    @Test
    public void testHasApplication() {
        System.out.println("hasApplication");
        Application a = null;
        ApplicationRegister instance = new ApplicationRegister();
        boolean expResult = false;
        boolean result = instance.hasApplication(a);
        assertEquals(expResult, result);

    }

    /**
     * Test of lstApplicationToString method, of class ApplicationRegister.
     */
    @Test
    public void testLstApplicationToString() {
        System.out.println("lstApplicationToString");
        ApplicationRegister instance = new ApplicationRegister();
        instance.addApplication(a);
        ArrayList<String> expResult = new ArrayList<String>();
        String asd = "Application: Descrição";
        expResult.add(asd);
        ArrayList<String> result = instance.lstApplicationToString();
        assertEquals(expResult, result);

    }

    /**
     * Test of obterArray method, of class ApplicationRegister.
     */
    @Test
    public void testObterArray() {
        System.out.println("obterArray");
        ApplicationRegister instance = new ApplicationRegister();
        instance.addApplication(a);
        Application[] expResult = new Application[]{a};
        Application[] result = instance.obterArray();
        assertArrayEquals(expResult, result);

    }

//    /**
//     * Test of getApplicationNome method, of class ApplicationRegister.
//     */
//    @Test
//    public void testGetApplicationByName() {
//        System.out.println("getApplicationNome");
//        String name = "aaa";
//        Application expResult = a;
//        ApplicationRegister array = new ApplicationRegister();
//        array.addApplication(a);
//        Application result = array.getApplicationByName(name);
//        assertEquals(expResult, result);
//    }

    /**
     * Test of getApplicationList method, of class ApplicationRegister.
     */
    @Test
    public void testGetApplicationList() {
        System.out.println("getApplicationList");
        ApplicationRegister instance = new ApplicationRegister();
        Application b = new Application("Decrição B", 300, 200);
        instance.addApplication(a);
        instance.addApplication(b);
        ArrayList<Application> expResult = new ArrayList<>();
        expResult.add(a);
        expResult.add(b);
        ArrayList<Application> result = instance.getApplicationList();
        assertEquals(expResult, result);
    }

  /**
   * Test of the method getApplicationOfFae, on the class ApplicationRegister
   */
    @Test
    public void testGetApplicationOfFae() {
        System.out.println("getApplicationOfFae");
        ApplicationRegister instance = new ApplicationRegister();
        ArrayList<Application> result;
        Application app1 = new Application(0,"hey",100,50);
        Application app2 = new Application(1,"descricao",30,40);
        Exhibition e = new Exhibition("exposicaoTest","hi","20","30","porto");
        e.getApplicationRegister().addApplication(app1);
        e.getApplicationRegister().addApplication(app2);
        FAE f1 =  new FAE("FAE1","fae1@centro.pt","fae1@centro.pt","password");
        FAE f2 =  new FAE("FAE1","fae2@centro.pt","fae2@centro.pt","password");
        e.getFAEsRegister().addFAE(f1);
        e.getFAEsRegister().addFAE(f2);
        Decision d1 = new Decision(f1,3,4,2,1,"porque sim");
        Decision d2 = new Decision(f2,5,5,5,5,"nice");
        app1.getDecisionRegister().addDecision(d1);
        app1.getDecisionRegister().addDecision(d2);
        Decision d3 = new Decision(f2,0,0,0,0,"pessimo");
        Decision d4 = new Decision(f1,2,3,3,2,"meh");
        app2.getDecisionRegister().addDecision(d3);
        app2.getDecisionRegister().addDecision(d4);
        result = instance.getApplicationOfFae(e, f2.getUsername());
        assertEquals(2,result.size());
        System.out.println(result.toString());
        assertEquals(app1,result.get(0));
        assertEquals(app2,result.get(1));
    }
}
