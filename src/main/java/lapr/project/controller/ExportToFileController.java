package lapr.project.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import lapr.project.model.FairCenter;

/**
 *
 * @author Manuel Sambade
 */
public class ExportToFileController {
    
        
    private static FairCenter fair_center;
    
    private static AssignStandController ctrl;
    
    /**
     * Constructor
     * @param fair_center 
     */
    public ExportToFileController (FairCenter fair_center){
        ExportToFileController.fair_center = fair_center;
        ctrl = new AssignStandController(ExportToFileController.fair_center);
    }
    
    //method to handle the assignement of stands to applications
    private void assignStands() {
        
       for(int i=0; i < ExportToFileController.fair_center.getEventRegister().getEventList().size(); i++) { 
           
            ctrl.assignStands(i);
       }
    }
    
    /**
     * Save data to file
     * @param path
     * @return boolean
     */
    public boolean save(String path) {
        
        this.assignStands();
        
        try {
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream(path));
            try {
                out.writeObject(fair_center);
            } finally {
                out.close();
            }
            
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    
}
