/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.ArrayList;
import lapr.project.model.Application;
import lapr.project.model.Decision;
import lapr.project.model.Event;
import lapr.project.model.FAE;
import lapr.project.model.FairCenter;

/**
 *
 * @author Ricardo Pinto
 */
public class TestTheDifferenceBetweenMeanDeviationController {

    private static FairCenter fairCenter;

    /**
     * Constructor
     *
     * @param fairCenter
     */
    public TestTheDifferenceBetweenMeanDeviationController(FairCenter fairCenter) {
        TestTheDifferenceBetweenMeanDeviationController.fairCenter = fairCenter;
    }

    /**
     *
     * @param index
     * @return mean rating for fae
     */
    public double getFaeMeanRating(FAE f) {
        int decisionCounter = 0;
        double sum = 0;
        for (Event e : fairCenter.getEventRegister().getEventList()) {
            for (Application c : e.getApplicationRegister().getApplicationList()) {
                for (Decision d : c.getDecisionRegister().getLstdecioes()) {
                    if (d.getDecisionFAE().equals(f)) {//comparing by username
                        decisionCounter++;
                        sum = sum + d.getFinalMean();
                    }
                }
            }
        }
        if (decisionCounter > 0) {
            return sum / decisionCounter;
        } else {
            return 0;
        }
    }

    /**
     * Obtain fae list
     *
     * @return fae list(array list)
     */
    public ArrayList<FAE> getFAEList() {
        return fairCenter.getFAEList();
    }

    public double getFaeDeviationMean(FAE f, ArrayList<Application> appFaeList, int sub) {
        double cont = 0;
        ArrayList<Decision> decisionFaeList = new ArrayList<>();
        for (Application a : appFaeList) {
            for (Decision d : a.getDecisionRegister().getLstdecioes()) {
                if (d.getDecisionFAE().equals(f)) {
                    decisionFaeList.add(d);
                }
            }
        }
        for (int i = 0; i < decisionFaeList.size(); i++) {
            double decisionFinalMean = decisionFaeList.get(i).calculateFinalMean();
            double diference = Math.abs(decisionFinalMean - getFaeMeanRating(f));
            cont = cont + diference;
        }
        return cont / sub;
    }

    public double getFaeStandardDeviation(FAE f, ArrayList<Application> appFaeList, int sub) {
        double cont = 0;
        ArrayList<Decision> decisionFaeList = new ArrayList<>();
        for (Application a : appFaeList) {
            for (Decision d : a.getDecisionRegister().getLstdecioes()) {
                if (d.getDecisionFAE().equals(f)) {
                    decisionFaeList.add(d);
                }
            }
        }
        for (int i = 0; i < decisionFaeList.size(); i++) {
            double decisionFinalMean = decisionFaeList.get(i).calculateFinalMean();
            double diference = Math.abs(decisionFinalMean - getFaeMeanRating(f));
            cont = cont + (diference * diference);
        }
        return Math.sqrt(cont / sub);
    }

    public double getObservedValue(FAE f, ArrayList<Application> appFaeList, int sub) {
        double dividend = getFaeDeviationMean(f, appFaeList, sub) - 1;
        double divisor = Math.sqrt(getFaeStandardDeviation(f, appFaeList, sub) / appFaeList.size());
        return dividend / divisor;
    }

    public String getDecisionAlert(int selectedIndex, FAE f, ArrayList<Application> appFaeList, int sub) {
        double observedValue = getFaeStandardDeviation(f, appFaeList, sub);
        double zc;
        if (selectedIndex == 0) {
            zc = 2.3263;
        } else if (selectedIndex == 1) {
            zc = 1.6449;
        } else {
            return "Error!";
        }
        if (observedValue>zc) {
            return "Yes";
        }else{
            return "No";
        }
    }
}
