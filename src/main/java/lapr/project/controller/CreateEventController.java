/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lapr.project.controller;

import java.util.ArrayList;
import lapr.project.model.Event;
import lapr.project.model.EventRegister;
import lapr.project.model.FairCenter;
import lapr.project.model.Organizer;
import lapr.project.model.UserRegister;


/**
 *
 * @author Pedro<1131485@isep.ipp.pt>
 */
public class CreateEventController {
    
    private  FairCenter fairCenter;

    /**
     * constructor of this controller
     * @param f FairCenter

     */
    public CreateEventController(FairCenter f) {

        this.fairCenter = f;
    }
    
    /**
     * method that will get the Events Register to further work
     * @return Events Register
     */
    public EventRegister getEvents() {
        
        return fairCenter.getEventRegister();
    }
    
    /**
     * method that will create a new Event on the FairCenter
     * @param title
     * @param description
     * @param beginingDate
     * @param endDate
     * @param placeRealization
     * @param type
     * @return Event
     */
    public Event newEvent(String title,String description, String beginingDate, String endDate, String placeRealization, int type) {
        
        int index =  fairCenter.createNewEvent(title, description, beginingDate, endDate, placeRealization, type);
        
        return fairCenter.getEventRegister().getEvent(index);
    }
    
    /**
     * method which will show the Organizers List on the fair center
     * @return Organizers List
     */
    public ArrayList<Organizer> showOrganizersList() {
        
        UserRegister ur = this.fairCenter.getUsersRegister();
        
        ArrayList<Organizer> lo = new ArrayList<>();
        
        for(int i=0; i < ur.getSize();i++) {
            if(ur.getUser(i) instanceof Organizer) {
                lo.add((Organizer)ur.getUser(i));
            }
        }
        
        return lo;
    }
    
    /**
     * method that will choose a organizer 
     * @param index
     * @return Organizer
     */
    public Organizer chooseOrganizer(int index){
        
        Organizer o = (Organizer) fairCenter.getUsersRegister().getUser(index);
        
        return o;
    }
    
    /**
     * method that will define a organizer to a certain event
     * @param o Organizer
     * @param e Event
     * @return 
     */
    public boolean defineOrganizer(Organizer o, Event e) {
    
        return e.setOrganizer(o);
    }
}
