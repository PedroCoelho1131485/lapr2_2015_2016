/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.ArrayList;
import lapr.project.model.Application;
import lapr.project.model.Decision;
import lapr.project.model.Event;
import lapr.project.model.EventRegister;
import lapr.project.model.FAE;
import lapr.project.model.FAEsRegister;
import lapr.project.model.FairCenter;

/**
 *
 * @author Ricardo Pinto
 */
public class DecideApplicationController {

    private FairCenter fairCenter;

    public DecideApplicationController(FairCenter fairCenter, int FaeKnowledge,
            int AppAdequacy, int InvitationQuantity, int Overall, String text,
            int eventIndex, int appIndex) {

        this.fairCenter = fairCenter;
        FAEsRegister fr = fairCenter.getEventRegister().getEvent(eventIndex).getFAEsRegister();
        FAE fae = fr.getFAE(fr.getFAEByUsername(LoginController.getUsername()));
        Event e = fairCenter.getEventRegister().getEvent(eventIndex);
        Application app = e.getApplicationRegister().getApplication(appIndex);
        for (Decision d : app.getDecisionRegister().getLstdecioes()) {
            if ((d.getDecisionFAE()).equals(fae)) {
                d.setAppAdequacy(AppAdequacy);
                d.setFaeKnowledge(FaeKnowledge);
                d.setInvitations(InvitationQuantity);
                d.setJustification(text);
                d.setOverall(Overall);
                double finalMean = d.calculateFinalMean();
                d.setFinalMean(finalMean);
            }
        }
    }

    public DecideApplicationController(FairCenter fairCenter) {
        this.fairCenter = fairCenter;
    }

    public ArrayList<Event> ListEvents() {
        EventRegister er = this.fairCenter.getEventRegister();

        return er.getEventList();
    }
}
