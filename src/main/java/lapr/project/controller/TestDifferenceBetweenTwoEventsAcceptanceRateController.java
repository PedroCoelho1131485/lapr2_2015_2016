/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import static java.lang.Math.sqrt;
import lapr.project.model.Application;
import lapr.project.model.Event;
import lapr.project.model.FairCenter;

/**
 *
 * @author Ricardo Pinto
 */
public class TestDifferenceBetweenTwoEventsAcceptanceRateController {

    private static FairCenter fairCenter;

    public TestDifferenceBetweenTwoEventsAcceptanceRateController(FairCenter fairCenter) {
        TestDifferenceBetweenTwoEventsAcceptanceRateController.fairCenter = fairCenter;
    }

    public String getEventName(int eventIndex) {
        Event event = fairCenter.getEventRegister().getEvent(eventIndex);
        return event.getTitle();
    }

    public String getProportion(int eventIndex) {
        Event e = fairCenter.getEventRegister().getEvent(eventIndex);
        int proportionCont = 0;
        for (Application a : e.getApplicationRegister().getApplicationList()) {
            if (a.getFinalAcceptance() == 1) {
                proportionCont++;
            }
        }
        return Integer.toString(proportionCont);
    }

    public String getValue(int event1Index, int event2Index) {
        Event e1 = fairCenter.getEventRegister().getEvent(event1Index);
        Event e2 = fairCenter.getEventRegister().getEvent(event2Index);
        int p1 = Integer.parseInt(getProportion(event1Index));
        int p2 = Integer.parseInt(getProportion(event2Index));
        double Event1AcptRate = e1.getAcceptanceRate();
        double Event2AcptRate = e2.getAcceptanceRate();
        double dividend = Event1AcptRate - Event2AcptRate;
        double divisor = sqrt(((Event1AcptRate * (1 - Event1AcptRate)) / p1) + ((Event2AcptRate * (1 - Event2AcptRate)) / p2));
        return String.valueOf(dividend / divisor);
    }

    public String getCriticalValue(int sigLevelIndex) {
        double zc;
        if (sigLevelIndex == 0) {
            zc = 2.5758;
            return String.valueOf(zc);
        } else if(sigLevelIndex == 1){
            zc = 1.9600;
            return String.valueOf(zc);
        }else{
            return "Error";
        }
    }

    public String getDiference(String Critical, String Value) {
        double critical = Double.parseDouble(Critical);
        double value = Double.parseDouble(Value);
        if (value<-critical || value>critical) {
            return "Yes";
        }else {
            return "No";
        }
    }
    
    
}
