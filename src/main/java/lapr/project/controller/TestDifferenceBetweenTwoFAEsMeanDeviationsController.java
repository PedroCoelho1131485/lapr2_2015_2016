/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.ArrayList;
import lapr.project.model.Application;
import lapr.project.model.Decision;
import lapr.project.model.Event;
import lapr.project.model.FAE;
import lapr.project.model.FairCenter;

/**
 *
 * @author Ricardo Pinto
 */
public class TestDifferenceBetweenTwoFAEsMeanDeviationsController {

    private static FairCenter fairCenter;

    /**
     *
     * @param fairCenter
     */
    public TestDifferenceBetweenTwoFAEsMeanDeviationsController(FairCenter fairCenter) {
        TestDifferenceBetweenTwoFAEsMeanDeviationsController.fairCenter = fairCenter;
    }

    /**
     *
     * @return fae's name
     */
    public String[] getFaeNames() {
        ArrayList<FAE> faeList = fairCenter.getFAEList();
        int n = faeList.size();
        String[] faeNames = new String[n];
        for (int i = 0; i < n; i++) {
            faeNames[i] = faeList.get(i).getName();
        }
        return faeNames;
    }

    /**
     *
     * @param fae
     * @return Number of Submissions of fae
     */
    public int getNumberSub(FAE fae) {
        int sub = 0;
        for (Event e : fairCenter.getEventRegister().getEventList()) {
            for (Application a : e.getApplicationRegister().getApplicationList()) {
                for (Decision d : a.getDecisionRegister().getLstdecioes()) {
                    if (d.getDecisionFAE().equals(fae)) {
                        sub++;
                    }
                }
            }
        }
        return sub;
    }

    /**
     *
     * @param fae
     * @return Application of FAE
     */
    public ArrayList<Application> getApplicationFAE(FAE fae) {
        ArrayList<Application> applicationFaeList = new ArrayList<>();
        for (Event e : fairCenter.getEventRegister().getEventList()) {
            for (Application a : e.getApplicationRegister().getApplicationList()) {
                for (Decision d : a.getDecisionRegister().getLstdecioes()) {
                    if (d.getDecisionFAE().equals(fae)) {
                        if (!applicationFaeList.contains(a)) {
                            applicationFaeList.add(a);
                        }
                    }
                }
            }
        }
        return applicationFaeList;
    }

    /**
     *
     * @param f
     * @return Deviations mean
     */
    public double getFaeDeviationMean(FAE f) {
        double cont = 0;
        ArrayList<Decision> decisionFaeList = new ArrayList<>();
        for (Application a : getApplicationFAE(f)) {
            for (Decision d : a.getDecisionRegister().getLstdecioes()) {
                if (d.getDecisionFAE().equals(f)) {
                    decisionFaeList.add(d);
                }
            }
        }
        for (int i = 0; i < decisionFaeList.size(); i++) {
            double decisionFinalMean = decisionFaeList.get(i).calculateFinalMean();
            double diference = Math.abs(decisionFinalMean - getFaeMeanRating(f));
            cont = cont + diference;
        }
        if (getNumberSub(f) == 0) {
            return 0;
        } else {
            return cont / getNumberSub(f);
        }
    }

    /**
     *
     * @param index
     * @return mean rating for fae
     */
    public double getFaeMeanRating(FAE f) {
        int decisionCounter = 0;
        double sum = 0;
        for (Event e : fairCenter.getEventRegister().getEventList()) {
            for (Application c : e.getApplicationRegister().getApplicationList()) {
                for (Decision d : c.getDecisionRegister().getLstdecioes()) {
                    if (d.getDecisionFAE().equals(f)) {//comparing by username
                        decisionCounter++;
                        sum = sum + d.getFinalMean();
                    }
                }
            }
        }
        if (decisionCounter > 0) {
            return sum / decisionCounter;
        } else {
            return 0;
        }
    }

    /**
     *
     * @param sigLevelIndex
     * @return Critical Value
     */
    public String getCriticalValue(int sigLevelIndex) {
        double zc;
        if (sigLevelIndex == 0) {
            zc = 2.5758;
            return String.valueOf(zc);
        } else if (sigLevelIndex == 1) {
            zc = 1.9600;
            return String.valueOf(zc);
        } else {
            return "Error";
        }
    }

    /**
     *
     * @return Observed value of test statistic
     */
    public double getObservedValue(FAE f1, FAE f2) {
        double dividend = getFaeDeviationMean(f1) - getFaeDeviationMean(f2);
        double divisor = Math.sqrt((getFaeStandardDeviation(f1) / getNumberSub(f1)) + (getFaeStandardDeviation(f2) / getNumberSub(f2)));
        return dividend / divisor;
    }

    /**
     *
     * @param f
     * @return Standard deviation
     */
    public double getFaeStandardDeviation(FAE f) {
        double cont = 0;
        ArrayList<Decision> decisionFaeList = new ArrayList<>();
        for (Application a : getApplicationFAE(f)) {
            for (Decision d : a.getDecisionRegister().getLstdecioes()) {
                if (d.getDecisionFAE().equals(f)) {
                    decisionFaeList.add(d);
                }
            }
        }
        for (int i = 0; i < decisionFaeList.size(); i++) {
            double decisionFinalMean = decisionFaeList.get(i).calculateFinalMean();
            double diference = Math.abs(decisionFinalMean - getFaeMeanRating(f));
            cont = cont + (diference * diference);
        }
        return Math.sqrt(cont / getNumberSub(f));
    }

    public String getDiference(FAE f1, FAE f2, int selectedIndex) {
        double observedValue = getFaeStandardDeviation(f1);
        double zc;
        if (selectedIndex == 0) {
            zc = 2.3263;
        } else if (selectedIndex == 1) {
            zc = 1.6449;
        } else {
            return "Error!";
        }
        if (observedValue < -zc || observedValue > zc) {
            return "Yes";
        } else {
            return "No";
        }
    }
}
