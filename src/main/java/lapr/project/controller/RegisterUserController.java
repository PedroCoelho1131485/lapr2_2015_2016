/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import javax.swing.JOptionPane;
import lapr.project.model.FairCenter;
import lapr.project.model.User;
import lapr.project.model.UserRegister;
import lapr.project.ui.RegisterUserUI;

/**
 *
 * @author Ricardo Pinto
 */
public class RegisterUserController {

    private RegisterUserUI framePai;
    private UserRegister userRegister;
    private FairCenter fairCenter;
    private String name; 
    private String email;
    private String username;
    private String hashedPassword;

    public RegisterUserController(FairCenter f, String name, String email, String username, char[] charPassword) {
        checkPassword(charPassword);
        this.name = name;
        this.email = email;
        this.username = username;
        this.hashedPassword = User.encrypt(charPassword);
        this.fairCenter = f;
        userRegister = fairCenter.getUsersRegister();

        if (checkPassword(charPassword) == true) {
            User user = new User(this.name,this.email, this.username, this.hashedPassword);
            userRegister.addUser(user);
            JOptionPane.showMessageDialog(framePai,
                    "The user registation was made with sucess",
                    "Sucess!",
                    JOptionPane.PLAIN_MESSAGE);
            userRegister.printUserList();
        } else if (checkPassword(charPassword) == false) {
            JOptionPane.showMessageDialog(framePai,
                    "The user password must include at least a number, both upper and lower case characters and a punctuation mark (“,”, “.”, “;”, “:” or “-“).",
                    "Atention!",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    public static boolean checkPassword(char[] charPassword) {
        String downLetras = String.copyValueOf(charPassword).replaceAll("[^a-z]", "");
        String pontos = String.copyValueOf(charPassword).replaceAll("[^. ; , : -]", "");
        String numeros = String.copyValueOf(charPassword).replaceAll("[^0-9]", "");
        String upLetras = String.copyValueOf(charPassword).replaceAll("[^A-Z]", "");
        if (!"".equals(downLetras) && !"".equals(pontos) && !"".equals(numeros) && !"".equals(upLetras)) {
            return true;
        } else {
            return false;
        }
    }
}
