/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import lapr.project.model.Application;
import lapr.project.model.FairCenter;

/**
 *
 * @author Manuel Sambade
 */
public class SubmitApplicationController {

    private static FairCenter fair_center;

    /**
     * Constructor
     *
     * @param fair_center
     */
    public SubmitApplicationController(FairCenter fair_center) {
        SubmitApplicationController.fair_center = fair_center;

    }

    /**
     * Creates new application with data provided from user
     * @param description
     * @param boothArea
     * @param inviteQt
     * @return Application
     */
    public Application createApplication(String description, int boothArea, int inviteQt){
        Application app = new Application(description, boothArea, inviteQt);
        return app;
    }
    
    /**
     * Allows user to submit an application
     *
     * @param index
     * @param app
     * @return true if app is submitted
     */
    public boolean submitApplication(int index, Application app) {
        
        if (fair_center.getEventRegister().getEvent(index).getApplicationRegister().hasApplication(app) == false) {
            fair_center.getEventRegister().getEvent(index).getApplicationRegister().addApplication(app);
            return true;

        }
        
        return false;

    }

}
