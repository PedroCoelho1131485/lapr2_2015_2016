/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import lapr.project.model.Application;
import lapr.project.model.FairCenter;

/**
 *
 * @author Manuel Sambade
 */
public class ChangeOrRemoveApplicationController {

    private static FairCenter fair_center;

    /**
     * Constructor
     *
     * @param fair_center
     */
    public ChangeOrRemoveApplicationController(FairCenter fair_center) {
        ChangeOrRemoveApplicationController.fair_center = fair_center;

    }

    /**
     * Removes the application
     *
     * @param eventIndex
     * @param appIndex
     */
    public void removeApplication(int eventIndex, int appIndex) {

        fair_center.getEventRegister().getEvent(eventIndex).getApplicationRegister()
                .removeApplication(fair_center.getEventRegister().getEvent(eventIndex)
                        .getApplicationRegister().getApplication(appIndex));

    }

    /**
     * Change application's description
     *
     * @param eventIndex
     * @param appIndex
     * @param new_description
     */
    public void changeApplicationDescription(int eventIndex, int appIndex, String new_description) {
        fair_center.getEventRegister().getEvent(eventIndex).getApplicationRegister()
                .getApplication(appIndex).setDescription(new_description);
    }

    /**
     * Change application's booth area
     *
     * @param eventIndex
     * @param appIndex
     * @param new_boothAreaString
     */
    public void changeApplicationBoothArea(int eventIndex, int appIndex, String new_boothAreaString) {
        int boothArea = Integer.parseInt(new_boothAreaString);
        fair_center.getEventRegister().getEvent(eventIndex).getApplicationRegister()
                .getApplication(appIndex).setBoothArea(boothArea);
    }

    /**
     * Change application's invites number
     *
     * @param eventIndex
     * @param appIndex
     * @param new_invitesString
     */
    public void changeApplicationInvitesQt(int eventIndex, int appIndex, String new_invitesString) {
        int invites = Integer.parseInt(new_invitesString);
        fair_center.getEventRegister().getEvent(eventIndex).getApplicationRegister()
                .getApplication(appIndex).setInvites(invites);
    }

    /**
     * Get current app data
     *
     * @param eventIndex
     * @param appIndex
     * @return
     */
    public String getCurrentAppData(int eventIndex, int appIndex) {
        Application app = fair_center.getEventRegister().getEvent(eventIndex).getApplicationRegister()
                .getApplication(appIndex);
        String current = String.format(
                "Description: " + app.getDescription()
                + "%nBooth Area: " + Integer.toString(app.getBoothArea())
                + "%nInvites Number: " + Integer.toString(app.getInvites()));
        return current;
    }

}
