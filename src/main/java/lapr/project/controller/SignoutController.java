/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import javax.swing.JOptionPane;
import lapr.project.model.FairCenter;
import lapr.project.model.UserRole;
import lapr.project.ui.MainMenuUI;
import lapr.project.ui.SignoutUI;

/**
 *
 * @author Ricardo Pinto
 */
public class SignoutController {

    private FairCenter fairCenter;

    public SignoutController(FairCenter fairCenter) {
        int returnValue = -1;
        String[] buttons = {"Yes", "No"};
        returnValue = JOptionPane.showOptionDialog(null, "You are about to sign out, are you sure?", "Attention",
                JOptionPane.WARNING_MESSAGE, 0, null, buttons, buttons[0]);
        if (buttons[returnValue].equals("Yes")) {
            this.fairCenter = fairCenter;
            MainMenuUI.role = UserRole.NOTREG;
            JOptionPane.showMessageDialog(null, "Signed out with sucess!");
        }else{
             JOptionPane.showMessageDialog(null, "The signed out operation was canceled");
        }
    }
}
