/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lapr.project.controller;

import lapr.project.model.Application;
import lapr.project.model.ApplicationRegister;
import lapr.project.model.Event;
import lapr.project.model.FairCenter;
import lapr.project.model.Stand;
import lapr.project.model.StandsRegister;

/**
 *
 * @author Pedro<1131485@isep.ipp.pt>
 */
public class AssignStandController {
    
    private static FairCenter fairCenter;
    
    //atribute to id a new stand to create, without entering with conflict the other existing stands
    private static int idStand;
    
    /**
     * constructor of this controller
     * @param f FairCenter
     */
    public AssignStandController(FairCenter f) {
        
        AssignStandController.fairCenter = f;
        
    }
   
    /**
     * method that will create a random stand to add to a event and then to be assigned to applications with no stands
     * @param indexEvent
     * @return stand
     */
    
    private Stand createStands(int indexEvent) {
        
        Event e = fairCenter.getEventRegister().getEvent(indexEvent);
        
        AssignStandController.idStand = e.getStandsRegister().getStandsList().size();
        
        AssignStandController.idStand++;
        
        String description = "Stand" + (AssignStandController.idStand);
        
        int area = (int)(Math.random() * 100 + 1) ; //will represent a random number between 1 and 100
        
        Stand s = new Stand(description,area);
        
        e.getStandsRegister().addStand(s);
        
        return s;
    }
    
    /**
     * method tbat assign stands if an application it hasnt already, just for one event
     * @param indexEvent 
     */
    public void assignStands(int indexEvent) {
        
        AssignStandController.idStand = 0;
        
        Event e = fairCenter.getEventRegister().getEvent(indexEvent);
        
        int a;
        
        ApplicationRegister ar = e.getApplicationRegister();
        
        StandsRegister sr = e.getStandsRegister();
        
        int s =0;
        
        Application app;
        
        boolean assigned = false;
        
        Stand stand;
        
        for(a=0; a < ar.getApplicationList().size(); a++) {
            
            app = ar.getApplication(a);
            
            if(!app.hasStandAssigned()) {
                while(!assigned && s < sr.getStandsList().size() ) {

                      if(!sr.getStand(s).isStandAssigned()) {
                          app.assignStand(sr.getStand(s));
                          sr.getStand(s).changeStandToAssigned();
                          assigned=true;
                      }

                       s++;
                    }
                    if(!assigned) {
                        stand = this.createStands(indexEvent);
                        app.assignStand(stand);
                        stand.changeStandToAssigned();
                        assigned = true;
                    }
                
                    s = 0;
                    assigned = false;
                }
                
             
            }
        }
    
    
}
