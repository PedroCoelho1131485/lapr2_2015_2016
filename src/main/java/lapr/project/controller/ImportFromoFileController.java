package lapr.project.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import lapr.project.model.FairCenter;

/**
 *
 * @author Manuel Sambade
 */
public class ImportFromoFileController {

    private static FairCenter fairCenter;

    public ImportFromoFileController() {

    }

    /**
     * Import fair center by path
     *
     * @return
     */
    public FairCenter importFile() {
        JFileChooser fChooser = new JFileChooser();
        int returnVal = fChooser.showOpenDialog(fChooser);

        if (returnVal == JFileChooser.APPROVE_OPTION) { //0 for approve , 1 for cancel , only two options
            File file = fChooser.getSelectedFile();
            String path = file.getPath();

            if (path.contains(".bin") || path.contains(".dat")) {

                int resultOption = JOptionPane.showConfirmDialog(null, "Are you sure that you want to import the data from this file?", "Confirm operation?", JOptionPane.YES_NO_OPTION);

                if (resultOption == JOptionPane.YES_OPTION) {

                    try {
                        try (ObjectInputStream in = new ObjectInputStream(
                                new FileInputStream(path))) {
                            fairCenter = (FairCenter) in.readObject();
                            JOptionPane.showMessageDialog(null,"All data imported with sucess","Data imported!",JOptionPane.INFORMATION_MESSAGE);
                        }
                        return fairCenter;

                    } catch (IOException | ClassNotFoundException ex) {
                        System.out.println(ex.toString());

                    }

                }
            }else JOptionPane.showMessageDialog(null, "File isnt the type : .bin","Wrong file",JOptionPane.ERROR_MESSAGE);
        }

        return null;
    }
}
