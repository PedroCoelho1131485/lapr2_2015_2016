/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lapr.project.controller;


import java.io.File;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import lapr.project.model.Event;
import lapr.project.model.FAE;
import lapr.project.model.FairCenter;
import lapr.project.model.Application;
import lapr.project.model.Decision;
import lapr.project.model.Keyword;
import lapr.project.model.Organizer;
import lapr.project.model.Stand;
import lapr.project.model.User;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Pedro<1131485@isep.ipp.pt>
 */
public class ImportEventsDataController {
    
    /**
     * atribute fair center
     */
    private static FairCenter fairCenter;
    private static String titleEvent;
    
    /** 
     * atributes - tags used to decode on xml
     */
    private final String eventTag = "event";
    private final  String titleTag = "title";
    private final String standsTag = "stands";
    private final  String standTag = "stand";
    private final  String descriptionTag = "description";
    private final  String areaTag = "area";
    private final  String faeListTag = "FAESet";
    private final  String userTag = "user";
    private final  String nameTag = "name";
    private final  String emailTag = "email";
    private final  String usernameTag = "username";
    private final  String pwTag = "password";
    private final  String applicationListTag = "applicationSet";
    private final  String applicationTag = "application";
    private final String acceptedTag = "accepted";
    private final  String boothAreaTag = "boothArea";
    private final String invitesQuantityTag = "invitesQuantity";
    private final  String reviewsTag = "reviews";
    private final  String reviewTag = "review";
    private final  String textTag = "text";
    private final  String faeTopicKnowledgeTag = "faeTopicKnowledge";
    private final  String eventAdequacyTag = "eventAdequacy";
    private final  String inviteAdequacyTag = "inviteAdequacy";
    private final  String recommendationTag = "recommendation";
    private final String keywordsTag = "keywords";
    private final  String keywordTag = "keyword";

    /**
     * construtor of the current Controller
     * 
     * @param f FairCenter
     */
    public ImportEventsDataController(FairCenter f) {
        ImportEventsDataController.fairCenter = f;
    }
    
    /**
     * method that will return an Event that has a certain title
     * @param title
     * @return Event
     */
    private Event searchEventByTitle(String title) {
        
        ArrayList<Event> le = fairCenter.getEventRegister().getEventList();
        
        int i =0;
        
        boolean found = false;
        Event e = new Event("","","","","");
        while (!found && i < le.size() ) {
            if(le.get(i).getTitle().equals(title)) {
                e = le.get(i);
                found = true;
            }
            i++;
        }
        int pos = -1;
        if(!found) {
            if(title.contains("exposicão")) {
               pos =  fairCenter.createNewEvent(titleEvent, "","","","", 2);
                
            }
            else{
                pos = fairCenter.createNewEvent(titleEvent, "", "", "", "", 1);
            }
        }
        if(found) {
            return e;
        }
        else { 
         return fairCenter.getEventRegister().getEvent(pos);
        }
    }
    
    /**
     * method that will add one stand for a current event by creating a new object and recieving its information
     */
    private void createStand(String description, int area, Event e) {
        
        Stand s = new Stand(description,area);
        
        e.getStandsRegister().addStand(s);
    }
    
    /**
     * method that will create an application and to the respetive event
     */
    private void addApplication(String titleEvent, Application app) {
        
        Event e = this.searchEventByTitle(titleEvent);
        
        e.getApplicationRegister().addApplication(app);
    }
    
    /**
     * method that will return a FAE if it exist on the FAE register within the certain Event
     */
    private FAE searchFAEONEvent(String faeEmail, String faeUsername, String titleEvent) {
        
        Event e = this.searchEventByTitle(titleEvent);
        
        ArrayList<FAE> lf = e.getFAEsRegister().getFaeList();
        
        int i=0;
        FAE f = null;
        boolean found = false;
        
        while(!found && i < lf.size()) {
            if(lf.get(i).getEmail().equals(faeEmail) && lf.get(i).getUsername().equals(faeUsername)) {
                f = lf.get(i);
                found = true;
            }
            i++;
        }

        return f;
    }
    
    /**
     * method that will receive an application and a decision, will add decision to the certain application
     */
    private void addDecisionTOApplication(Application app, Decision decision) {
        
        app.getDecisionRegister().addDecision(decision);
    }
    
    /**
     * method that will receive a keyword, which will be added to a certain application (received too)
     */
    private void addKeywordTOApplication(Application app, Keyword keyword) {
        
        app.getListKeywords().add(keyword);
    }
    
    /**
     * method that will add a FAE to the current Event
     */
    private void addFaeToEvent(FAE f, Event e) {
        
        e.getFAEsRegister().addFAE(f);
    }
    
    private void addFaeToFairCenter(FAE f) {
        
        if(!fairCenter.getUsersRegister().getUserList().contains((User)f)) {
            fairCenter.getUsersRegister().addUser(f);
        }
    }
    
    /**
     * method to import the needed data from a xml file to the fairCenter
     * @param filePath
     * @return 
     */
    public boolean importDataEvent(String filePath) {
 
        boolean flag = false;

        try{
            //start by opening the file containing the data
            File xmlFile = new File(filePath);//object that will work with file
            
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            Document doc = dBuilder.parse(xmlFile);
            
            doc.getDocumentElement().normalize();
            
            NodeList eList = doc.getElementsByTagName(eventTag);
            
            
            for(int i=0; i < eList.getLength(); i++){
                
                Node node = eList.item(i);
                
                //events
                if(node.getNodeType() == Node.ELEMENT_NODE) {
                    
                    Element eElement = (Element) node;
                    
 
                //get event with help of the title
                ImportEventsDataController.titleEvent = eElement.getElementsByTagName(titleTag).item(i).getTextContent();
                
                
                Event e = this.searchEventByTitle(titleEvent);

                Organizer o1 = new Organizer("majorOrganizer","major","major","general");
                Organizer o2 = new Organizer("default","default","default","default");
                
                fairCenter.getUsersRegister().addUser(o1);
                fairCenter.getUsersRegister().addUser(o2);
                
                //creating organizers just not to be creating an event without any organizers
                e.setOrganizer(o1);
                e.setOrganizer(o2);

                    //stands
                    int x=0;

                    //get every stand
                    while(x<eElement.getElementsByTagName(standTag).getLength()) {
                        
                        String description = eElement.getElementsByTagName(descriptionTag).item(x).getTextContent();
                      
                        int area = Integer.parseInt(eElement.getElementsByTagName(areaTag).item(x).getTextContent());

                        this.createStand(description, area, this.searchEventByTitle(titleEvent));
                        x++;
                    }
                    
                    

                
                }
             
                //users from the fae set
                NodeList faeList =  doc.getElementsByTagName(faeListTag);
                
                node = faeList.item(i);

                
                int x=0;
                
                if(node.getNodeType() == Node.ELEMENT_NODE) {
                    
                    Element eElement = (Element) node;
                
                    while(x < eElement.getElementsByTagName(userTag).getLength()) {
                        String name = eElement.getElementsByTagName(nameTag).item(x).getTextContent();
                        String email = eElement.getElementsByTagName(emailTag).item(x).getTextContent();
                        String username = eElement.getElementsByTagName(usernameTag).item(x).getTextContent();
                        String password = eElement.getElementsByTagName(pwTag).item(x).getTextContent();
                        FAE fae = new FAE(name,email,username,password);
                        Event ev = this.searchEventByTitle(titleEvent);
                        this.addFaeToEvent(fae, ev);
                        this.addFaeToFairCenter(fae);
                        x++;
                    }
                
            }

                //applicationSet
                
                NodeList applicationList  = doc.getElementsByTagName(applicationListTag);
                
                node = applicationList.item(i);
                
                
                int a=0;
                
                if(node.getNodeType() == Node.ELEMENT_NODE) {
                    
                    Element eElement = (Element) node;
                    //every application
                    while(a < eElement.getElementsByTagName(applicationTag).getLength()){
                        String txt = eElement.getElementsByTagName(acceptedTag).item(a).getTextContent();
                        int accepted = -1; 
                        
                        if("true".equals(txt)) {
                            accepted = 1;
                        }
                        else {
                            accepted = 0;
                        }

                        
                        String description = eElement.getElementsByTagName(descriptionTag).item(a).getTextContent();

                        int boothArea = Integer.parseInt(eElement.getElementsByTagName(boothAreaTag).item(a).getTextContent());

                        int invitesQuantity = Integer.parseInt(eElement.getElementsByTagName(invitesQuantityTag).item(a).getTextContent());

                        
                        //create an object for Application
                        
                        Application app = new Application(accepted,description,boothArea,invitesQuantity);
                        
                        //add the application to event
                        
                        this.addApplication(titleEvent, app);
                        
                        //create application then create review or decision to add to the application 
                            
                            //reviews
                            NodeList reviewsList = doc.getElementsByTagName(reviewsTag);
                            
                            Node another = reviewsList.item(a);

                            
                            if(node.getNodeType() == Node.ELEMENT_NODE) {
                                
                                Element element = (Element) another;
                            

                                int r=0;
                                while(r < element.getElementsByTagName(reviewTag).getLength()){
                                    String text = element.getElementsByTagName(textTag).item(r).getTextContent();

                                    int faeTopicKnowledge = Integer.parseInt(element.getElementsByTagName(faeTopicKnowledgeTag).item(r).getTextContent());

                                    int eventAdequacy = Integer.parseInt(element.getElementsByTagName(eventAdequacyTag).item(r).getTextContent());

                                    int inviteAdequacy = Integer.parseInt(element.getElementsByTagName(inviteAdequacyTag).item(r).getTextContent());

                                    int recommendation = Integer.parseInt(element.getElementsByTagName(recommendationTag).item(r).getTextContent());

          
                                    //getting fae username and email
                                    
                                    String faeUser = element.getElementsByTagName(usernameTag).item(r).getTextContent();

                                    
                                    String faeEmail = element.getElementsByTagName(emailTag).item(r).getTextContent();

                                    
                                    //use email and username to find fae on the event list and next that fae can be added on the assigment - decision
                                    
                                    //creating a decision and adding it to the said application 
                                    FAE fae = this.searchFAEONEvent(faeEmail, faeUser, titleEvent);
                                    if(fae !=null) {
                                        Decision decision = new Decision(fae,faeTopicKnowledge,eventAdequacy,inviteAdequacy,recommendation,text);
                                        this.addDecisionTOApplication(app, decision);

                                    }
                                    
                                    r++;
                                }
                                 
                                //keywords
                                
                                int k=0;
                                
                                NodeList keywordsList = doc.getElementsByTagName(keywordsTag);
                                
                                Node keyword = keywordsList.item(a);
                                
                                
                                if(node.getNodeType() == Node.ELEMENT_NODE) {
                    
                                    Element eKeyword = (Element) keyword;

                                    
                                    while(k < eKeyword.getElementsByTagName(keywordTag).getLength()) {
                                        
                                        String txtKeyword = eKeyword.getElementsByTagName(keywordTag).item(k).getTextContent();
                                        

                                        //will create Keyword and add to the application 
                                        
                                        Keyword kw = new Keyword(txtKeyword);
                                        
                                        this.addKeywordTOApplication(app, kw);
                                         
                                        k++;
                                    }
                                }
                            
                            }
                        a++;
                    }
                }
            
          }
            
            
            flag = true; //operation done
            
            
        }
        catch (Exception e) {
       
        }
        
        return flag;
    }

}