/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lapr.project.controller;

import java.util.ArrayList;
import lapr.project.model.Application;
import lapr.project.model.ApplicationRegister;
import lapr.project.model.Event;
import lapr.project.model.EventRegister;
import lapr.project.model.FairCenter;

/**
 *
 * @author Pedro<1131485@isep.ipp.pt>
 */
public class ListApplicationController {

    /**
     * Atribute : FairCenter
     */
    private FairCenter fairCenter;
    
    /**
     * constructor of the controller
     * @param f FairCenter
     */
    public ListApplicationController(FairCenter f) {
        this.fairCenter = f;
    }
    
    /**
     * method that returns the current Events list of the current FairCenter
     * @return List of Events
     */
    public ArrayList<Event> ListEvents() {
        
        EventRegister er = this.fairCenter.getEventRegister();
        
        return er.getEventList();
    }
    
    /**
     * method that will get the list of application of the event just selected
     * @param index position of the event
     * @return List of Application
     */
    public ArrayList<Application> chooseEvent(int index) {
        
        Event e = this.fairCenter.getEventRegister().getEvent(index);
        
        ApplicationRegister ar = e.getApplicationRegister();
        
        return ar.getApplicationList();
    }
}
