/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.ArrayList;
import lapr.project.model.Application;

import lapr.project.model.Event;
import lapr.project.model.EventRegister;
import lapr.project.model.Keyword;

/**
 *
 * @author Manuel Sambade
 */
public class ShowEventSubKeywordsController {

    /**
     * Constructor
     *
     */
    public ShowEventSubKeywordsController() {

    }

    /**
     * Allows converting event register to String[]
     *
     * @param reg
     * @return
     */
    public String[] eventRegToArrayString(EventRegister reg) {
        String[] eventNames = new String[reg.getEventList().size()];
        int counter = 0;
        for (Event event : reg.getEventList()) {
            eventNames[counter] = event.toString();
            counter++;
        }
        return eventNames;
    }

    /**
     * Obtain number of keywords
     *
     * @param e
     * @return
     */
    public int calculateNumberOfKeywords(Event e) {
        ArrayList<Keyword> keywords = new ArrayList<>();

        for (Application a : e.getApplicationRegister().getApplicationList()) {
            for (Keyword k : a.getListKeywords()) {
                if (!keywords.contains(k)) {
                    keywords.add(k);
                }

            }

        }
        return keywords.size();
    }

    /**
     * Calculates keyword frequency for an event
     *
     * @param e
     * @param keyword
     * @return
     */
    public double calculateKeywordFrequency(Event e, Keyword keyword) {
        int n = 0;
        ArrayList<Keyword> keywordsRegistered = new ArrayList<>();
        for (Application a : e.getApplicationRegister().getApplicationList()) {
            for (Keyword k : a.getListKeywords()) {
                keywordsRegistered.add(k);
                if (keyword.equals(k)) {
                    n++;
                }

            }
        }

        if (!keywordsRegistered.isEmpty()) {
            return (double) n / keywordsRegistered.size() * 100;

        }

        return 0;
    }

    /**
     * Sort frequency table by name of keyword
     *
     * @param data
     */
    public void alphaSort(String[][] data) {
        String tempStr;
        String tempStr2;

        String[] col1 = new String[data.length];
        String[] col2 = new String[data.length];
        for (int i = 0; i < col1.length; i++) {
            col1[i] = data[i][0];
            col2[i] = data[i][1];
        }

        for (int t = 0; t < col1.length - 1; t++) {
            for (int i = 0; i < col1.length - t - 1; i++) {
                if (col1[i + 1].compareTo(col1[i]) < 0) {
                    tempStr = col1[i];
                    tempStr2 = col2[i];
                    col1[i] = col1[i + 1];
                    col2[i] = col2[i + 1];
                    col1[i + 1] = tempStr;
                    col2[i + 1] = tempStr2;
                }
            }
        }

        for (int i = 0; i < col1.length; i++) {
            data[i][0] = col1[i];
            data[i][1] = col2[i];
        }

    }

}
