package lapr.project.controller;

import java.util.ArrayList;
import lapr.project.model.Application;
import lapr.project.model.Decision;
import lapr.project.model.Event;
import lapr.project.model.EventRegister;
import lapr.project.model.FAE;
import lapr.project.model.FairCenter;

/**
 *
 * @author Manuel Sambade
 */
public class ShowFAEMeanRatingController {

    private static FairCenter fairCenter;

    /**
     * Constructor
     *
     * @param fairCenter
     */
    public ShowFAEMeanRatingController(FairCenter fairCenter) {
        ShowFAEMeanRatingController.fairCenter = fairCenter;
    }

    /**
     * Obtain fae list
     *
     * @return fae list(array list)
     */
    public ArrayList<FAE> getFAEList() {
        return fairCenter.getFAEList();
    }

    /**
     * Obtain FAE's mean rating
     *
     * @param index
     * @return mean rating for fae
     */
    public double getFAEMeanRating(int index) {
        ArrayList<FAE> faeList = getFAEList();

        int decisionCounter = 0;
        double sum = 0;
        for (Event e : fairCenter.getEventRegister().getEventList()) {
            for (Application c : e.getApplicationRegister().getApplicationList()) {
                for (Decision d : c.getDecisionRegister().getLstdecioes()) {
                    if (d.getDecisionFAE().equals(faeList.get(index))) {//comparing by username
                        decisionCounter++;
                        sum = sum + d.getFinalMean();
                    }
                }
            }
        }
        if (decisionCounter > 0) {
            return sum / decisionCounter;

        } else {
            return 0;
        }

    }

    /**
     * method that return the calculated global mean rate for every submission
     * @return global mean rate
     */
    public double getGlobalMeanRate() {
        EventRegister er = fairCenter.getEventRegister();
        return er.getGlobalMeanRate();
    }
    
    /**
     * method that calculate and show the mean deviation between the FAE mean rating and the submission global rating
     * @param index of fae
     * @return 
     */
    public double[][] getMeanDeviationGlobalAndFAERating(int index) {
        
        double[][] table;
        
        double globalRate = this.getGlobalMeanRate();

        int numberDecisions = this.getNumberDecisions(index);
        
        table = new double[numberDecisions][3];
         
        double[] meanRatesFae = this.getFAEMeanRates(index, numberDecisions);
        //transport the meanRate of FAE to the table create before
        
        for(int i = 0; i < numberDecisions; i++) {
           table[i][0] = (double)(i+1);
           table[i][1] = meanRatesFae[i]; 
        }
        
        //calculating the deviations for the second column of the table
        
        for(int l=0; l < numberDecisions; l++) {
            table[l][2] = Math.abs(globalRate - table[l][0]);
        }
        
        
        return table;
    }

    
    private int getNumberDecisions(int index) {
        
        int decisionCounter = 0;
        
        ArrayList<FAE> faeList = this.getFAEList();
            for (Event e : fairCenter.getEventRegister().getEventList()) {
            for (Application c : e.getApplicationRegister().getApplicationList()) {
                for (Decision d : c.getDecisionRegister().getLstdecioes()) {
                    if (d.getDecisionFAE().equals(faeList.get(index))) {//comparing by username
         
                        decisionCounter++;
                    }
                }
            }
        }
                
        return decisionCounter;
    }
    
    //method to get the mean rate of every fae
    private double[] getFAEMeanRates(int index, int decisionQt) {
        
        double[] rates = new double[decisionQt];
        int i=0;
        
        ArrayList<FAE> faeList = this.getFAEList();
         for (Event e : fairCenter.getEventRegister().getEventList()) {
            for (Application c : e.getApplicationRegister().getApplicationList()) {
                for (Decision d : c.getDecisionRegister().getLstdecioes()) {
                    if (d.getDecisionFAE().equals(faeList.get(index))) {//comparing by username
                         
                             rates[i] = d.getFinalMean();
                             i++;
                         }
                    }        
                }
            }
          
        
      
         return rates;
    }
}
