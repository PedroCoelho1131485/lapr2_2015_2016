package lapr.project.controller;

import java.util.ArrayList;
import lapr.project.model.Event;
import lapr.project.model.FairCenter;

/**
 *
 * @author Manuel Sambade
 */
public class EventAcceptanceRateController {

    private static FairCenter fair_center; 
    

    /**
     * Constructor
     *
     * @param fair_center
     */
    public EventAcceptanceRateController(FairCenter fair_center) {
        EventAcceptanceRateController.fair_center = fair_center;
        
    }

    /**
     * Obtain the list of events in format String[]
     *
     * @return String[] event_list
     */
    public ArrayList<Event> getEventList() {

        return EventAcceptanceRateController.fair_center.getEventRegister().getEventList();

    }

    
    /**
     * Obtain the acceptance rate for a specif event
     *
     * @param index
     * @return acceptance rate for the selected event
     */
    public double getEventAcceptanceRate(int index) {       
        return fair_center.getEventRegister().getEvent(index).getAcceptanceRate();
    }
    
    /**
     * Obtain the acceptance rate of all events
     * @return global acceptance rate
     */
    public double getGlobalAcceptanceRate(){
        return fair_center.getGlobalAcceptanceRate();
    }
    
    /**
     * Verify if one event's acceptance rate is over 50%
     * @param index
     * @return boolean
     */
    public boolean verifyRateIsGreaterThan50Percent(int index){
        double rate = getEventAcceptanceRate(index);
        return rate>0.5;
    }
}
