/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import javax.swing.JOptionPane;
import lapr.project.model.EventManager;
import lapr.project.model.FAE;
import lapr.project.model.FairCenter;
import lapr.project.model.Organizer;
import lapr.project.model.Participant;
import lapr.project.model.Representative;
import lapr.project.model.User;
import lapr.project.model.UserRole;
import lapr.project.ui.LoginUI;
import lapr.project.ui.MainMenuUI;

/**
 *
 * @author Ricardo Pinto
 */
public class LoginController implements UserRole {

    private LoginUI framePai;
    private FairCenter fairCenter;
    private static String username;

    public LoginController(FairCenter fairCenter, String username, char[] password) {
        LoginController.username = username;
        String hashedPassword = User.encrypt(password);
        this.fairCenter = fairCenter;
        for (User u : fairCenter.getUsersRegister().getUserList()) {
            if (LoginController.username.equals(u.getUsername())) {
                if (hashedPassword.equals(u.getHashedPassword())) {
                    if (u instanceof EventManager) {
                        MainMenuUI.role = UserRole.EVENTMANAGER;
                    } else if (u instanceof FAE) {
                        MainMenuUI.role = UserRole.FAE;
                    } else if (u instanceof Organizer) {
                        MainMenuUI.role = UserRole.ORGANIZER;
                    } else if (u instanceof Participant) {
                        MainMenuUI.role = UserRole.PARTICIPANT;
                    } else if (u instanceof Representative) {
                        MainMenuUI.role = UserRole.REPRESENTATIVE;
                    }
                }
            }
        }
    }

    /**
     * @return the username
     */
    public static String getUsername() {
        return username;
    }
}
