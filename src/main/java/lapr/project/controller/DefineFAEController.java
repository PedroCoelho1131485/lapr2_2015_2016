/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lapr.project.controller;

import java.util.ArrayList;
import lapr.project.model.Event;
import lapr.project.model.FAE;
import lapr.project.model.FairCenter;
import lapr.project.model.Organizer;
import lapr.project.model.User;
import lapr.project.model.UserRegister;

/**
 *
 * @author Pedro<1131485@isep.ipp.pt>
 */
public class DefineFAEController {

    private FairCenter fairCenter;
    
    /**
     * constructor of this controller
     * @param f FairCenter
     */
    public DefineFAEController(FairCenter f) {
        this.fairCenter = f;

    }
    
    //gets the organizer that has login to use this feature of the app
    public Organizer getOrganizer() {
        Organizer o = null;
        UserRegister ur = this.fairCenter.getUsersRegister();
        int i = ur.getOrganizerByUsername(LoginController.getUsername());
        
        if(i!=-1) {
            o = (Organizer) ur.getUser(i);
        }
        
        return o;
    }
    
    /**
     * method that will return the list of the Organizer
     * @param o
     * @return List of Events
     */
    public ArrayList<Event> showOrganizerEventsList(Organizer o) {
       
      if(o!= null) {  
        
        ArrayList<Event> organizersEvents = new ArrayList<>();
        
        int i;
        
        ArrayList<Event> allEvents = this.fairCenter.getEventRegister().getEventList();
        
        Event currentEvent;

        
        for(i=0;i < allEvents.size(); i++) {
            currentEvent = allEvents.get(i);
            
            if(currentEvent.getOrganizersRegister().getOrganizersList().contains(o)) {
                organizersEvents.add(currentEvent);
            }
        }
        
        return organizersEvents;
      }
      else {
          return null;
      }
    }
    
    /**
     * method that will return an event chosen by the user
     * @param index of the event chosen
     * @param le EventsList 
     * @return Event
     */
    public Event getEvent(int index, ArrayList<Event> le) {
        
        return le.get(index);
    }
    
    /**
     * method that will record or add the FAE to the certain Event
     * @param f FAE
     * @param e Event
     * @return boolean
     */
    public boolean recordFAE(FAE f, Event e) {
        
        String username = f.getUsername();
        String email = f.getEmail();
        boolean flag = false;
        int i=0;
        
        ArrayList<FAE> lFAE = e.getFAEsRegister().getFaeList();
        System.out.println(lFAE.toString());
        while(!flag && i < lFAE.size()) {
            if(lFAE.get(i).getEmail().equals(email) && lFAE.get(i).getUsername().equals(username)) {
                flag = true;
            }
            i++;
        }
        
        return flag;
    }
}
