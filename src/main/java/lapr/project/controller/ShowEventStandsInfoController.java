/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lapr.project.controller;

import java.util.ArrayList;
import lapr.project.model.Event;
import lapr.project.model.EventRegister;
import lapr.project.model.FairCenter;
import lapr.project.model.Stand;
import lapr.project.model.StandsRegister;

/**
 *
 * @author Pedro<1131485@isep.ipp.pt>
 */
public class ShowEventStandsInfoController {
    
    /**
     * Atribute which will represent the fair center at work
     */
    private static FairCenter fairCenter;

    /**
     * constructor of this controller
     * @param f
     */
    public ShowEventStandsInfoController(FairCenter f) {
        ShowEventStandsInfoController.fairCenter = f;
    }
    
    /**
     * method showListEvents
     * @return EventsList
     */
    public ArrayList<Event> showListEvents() {
        
        EventRegister er = ShowEventStandsInfoController.fairCenter.getEventRegister();
        return er.getEventList();
    }
    
    /**
     * method that will return a list of the current Events Stand
     * @param index
     * @return List of Stands
     */
    public ArrayList<Stand> showEventStands(int index) {
        
        Event e = ShowEventStandsInfoController.fairCenter.getEventRegister().getEvent(index);
       
        ArrayList<Stand> ls = e.getStandsRegister().getStandsList();
        return ls;
    }
    
    /**
     * method that will return the information about a selected stand
     * @param indexStand
     * @param indexEvent
     * @return 
     */
    public String getInformation(int indexStand, int indexEvent) {
        
        Event e = ShowEventStandsInfoController.fairCenter.getEventRegister().getEvent(indexEvent);
        
        Stand s = e.getStandsRegister().getStand(indexStand);
        return s.getInformation();
    }
    
    /**
     * method that returns the mean deviation of the area of the all stands on the event chosen by the user
     * @param indexEvent
     * @return 
     */
    
    public double getMeanEvent(int indexEvent) {
       
        StandsRegister sr = fairCenter.getEventRegister().getEvent(indexEvent).getStandsRegister();
        
        return sr.calculateMean();
    }
    
    /**
     * method that returns the standard deviation of the area of all the stands on the event chosen by the user
     * @param indexEvent
     * @return 
     */
    public double getStandardDeviation(int indexEvent) {
        
        StandsRegister sr = fairCenter.getEventRegister().getEvent(indexEvent).getStandsRegister();
        
        return sr.calculateStandardDeviation();
    }
    
    /**
     * method that returns a multi dimensional Array which represents the frequency table
     * @param indexEvent
     * @return 
     */
    public double[][] getFrequencyTable(int indexEvent) {
        
        StandsRegister sr = fairCenter.getEventRegister().getEvent(indexEvent).getStandsRegister();
        
        return sr.calculateAreaFrenquecies();
    }
}
