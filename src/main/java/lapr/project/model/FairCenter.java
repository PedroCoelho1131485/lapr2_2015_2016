package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Manuel Sambade
 */
public class FairCenter implements Serializable {

    private static final long serialVersionUID = 113;

    /**
     * Atribute events_register: represents events_register, which contains the
     * list of events
     */
    private EventRegister eventRegister;

    /**
     * Atribute users_register: represents users_register, which contains the
     * list of all users
     */
    private UserRegister userRegister;

    /**
     * Constructor
     */
    public FairCenter() {
        this.eventRegister = new EventRegister();
        this.userRegister = new UserRegister();
    }

    /**
     * Obtain Events Register, which contains the list of events
     *
     * @return events_register
     */
    public EventRegister getEventRegister() {
        return this.eventRegister;
    }

    /**
     * Obtain User Register, which contains the list of all the users
     *
     * @return users_register
     */
    public UserRegister getUsersRegister() {

        return this.userRegister;
    }

    /**
     * methods that will create a congress or a exihibition by the users choice
     * 1 - Congress 2 - Exihibiton
     *
     * @param title
     * @param description
     * @param beginingDate
     * @param endDate
     * @param placeRealization
     * @param type
     * @return of the index which this event was just created, to then return
     * the event to the controller
     *
     */
    public int createNewEvent(String title, String description, String beginingDate, String endDate, String placeRealization, int type) {

        int index; //if returns -1 an event was never created

        switch (type) {
            case 1:
                //Congress
                Congress c = this.newCongress(title, description, beginingDate, endDate, placeRealization);
                this.eventRegister.addEvent(c);
                index = this.eventRegister.getPosition(c);
                break;

            case 2:
                //Exhibition
                Exhibition ex = this.newExhibition(title, description, beginingDate, endDate, placeRealization);
                this.eventRegister.addEvent(ex);
                index = this.eventRegister.getPosition(ex);
                break;

            default:
                //if the variable type isnt the one request
                index = -1; //just a way to be sure that the method returns the expected result
                break;
        }

        return index;
    }

    /**
     * private method of the creation of a new Congress
     *
     * @param title
     * @param description
     * @param beginingDate
     * @param endDate
     * @param placeRealization
     * @return Congress
     */
    private Congress newCongress(String title, String description, String beginingDate, String endDate, String placeRealization) {

        return new Congress(title, description, beginingDate, endDate, placeRealization);
    }

    /**
     *
     * @param title
     * @param description
     * @param beginingDate
     * @param endDate
     * @param placeRealization
     * @return Exhibition
     *
     * private method that will create a new Exhibition object
     */
    private Exhibition newExhibition(String title, String description, String beginingDate, String endDate, String placeRealization) {

        return new Exhibition(title, description, beginingDate, endDate, placeRealization);
    }

    public double getGlobalAcceptanceRate() {
        int aproovedCounter = 0;
        int applicationTotal = 0;
        for (Event e : eventRegister.getEventList()) {
            for (Application a : e.getApplicationRegister().getApplicationList()) {
                if (a.getFinalAcceptance() == 1) {
                    aproovedCounter++;
                }
            }
        }
        for (Event e : eventRegister.getEventList()) {
            for (Application a : e.getApplicationRegister().getApplicationList()) {
                applicationTotal++;
            }

        }
        if (applicationTotal == 0) {
            return (double) 0;
        }
        return (double) aproovedCounter / applicationTotal;

    }

    /**
     * @param events_register list of events
     */
    public void setEventRegister(EventRegister eventRegister) {
        this.eventRegister = eventRegister;
    }

    /**
     * method that will instantiate a new Stand
     *
     * @param description
     * @param area
     * @return
     */
    public Stand createNewStand(String description, int area) {

        return new Stand(description, area);
    }

    /**
     * Obtain fae list
     *
     * @return fae list(array list)
     */
    public ArrayList<FAE> getFAEList() {
        ArrayList<FAE> faeList = new ArrayList<>();
        EventRegister eventRegister = getEventRegister();
        for (Event e : eventRegister.getEventList()) {
            for (Application c : e.getApplicationRegister().getApplicationList()) {
                for (Decision d : c.getDecisionRegister().getLstdecioes()) {
                    if (!faeList.contains(d.getDecisionFAE())) {
                        faeList.add(d.getDecisionFAE());
                    }

                }
            }
        }

        return faeList;
    }

    /**
     * 
     * @param name
     * @return Fae By Name
     */
    public FAE getFaeByName(String name) {
        int fae = 0;
        int cont = 0;
        ArrayList<FAE> faeList = getFAEList();
        for (FAE f : faeList) {
            if (!f.getName().equals(name)) {
                cont++;
            } else {
                fae = cont;
            }
        }
        return faeList.get(fae);
    }
}
