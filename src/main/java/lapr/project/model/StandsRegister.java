/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Pedro<1131485@isep.ipp.pt>
 */
public class StandsRegister implements Serializable{
   private static final long serialVersionUID = 120;
    /**
     * Contains every stand
     */
    private ArrayList<Stand> stand_list;
    
    /**
     * Constructor of the class StandsRegister
     */
    public StandsRegister() {
        this.stand_list = new ArrayList<>();
    }
    
    /**
     * Obtain the list of Stands
     * @return stand_list
     */
    public ArrayList<Stand> getStandsList() {
        
        return this.stand_list;
    }
    
    /**
     * Add stand to the list
     * 
     * @param stand
     */
    public void addStand(Stand stand) {
        
        if(!this.validateStand(stand)){
            this.stand_list.add(stand);
        }
    }
    
    /**
     * verifys if the stand given by user already exists on the list
     */
    private boolean validateStand(Stand s) {
        
        return this.stand_list.contains(s);
    }
    
    /**
     * Removes stand from the list
     * @param stand
     */
    public void removeStand(Stand stand) {
        
        if(this.validateStand(stand)){
            this.stand_list.remove(stand);
        }
    }
    
    /**
     * Returns a certain stand by a given index by the user
     * @param index
     * @return Stand
     */
    public Stand getStand(int index) {
        
        return this.stand_list.get(index);
    }
   
    
    /**
     * method that will calculate the stand Deviation of the stand Area
     * @return double
     */
    public double calculateStandardDeviation() {
        
        int n = this.getStandsList().size(); // current number of stands
        
        double sum = 0.0;
        
        for(int i=0; i < n; i++) {
            sum += this.calculateDistanceToMean(i);
        }
        
        double division = sum / n;
        
        return Math.sqrt(division);
    }
   
    /**
     * method that calculate the mean of the stand register
     * @return mean
     */
    public double calculateMean() {
        
        int sum = 0; 
        int totalStands = this.getStandsList().size();
        
        for(int i=0; i <  totalStands; i++) {
            sum += this.getStand(i).getArea();
        }
        
        
        return (double) sum / totalStands;
    }
    
    /**
     * method that will calculate the find the square of the distance of each area to the mean
     */
    private double calculateDistanceToMean(int index) {
       
        int currentArea = this.getStand(index).getArea();
        
        double currentMean = this.calculateMean();
        
        return Math.pow(Math.abs(currentMean - currentArea),2);
        
    }
    
    /**
     * method that calculates the mean deviation
     * @return double 
     */
    public double calculateMeanDeviation() {
        
        double mean = this.calculateMean();
        
        double sum = 0;
        
        int totalStands = this.getStandsList().size();
        
        for(int i=0; i < totalStands; i++) {
            sum +=  Math.abs(mean - this.getStand(i).getArea());
        }
        
        return sum / totalStands;
    }
    
    /**
     * method to calculate the frequencies of the areas of the stands 
     * @return frequencyTable
     */
    public double[][] calculateAreaFrenquecies() {
        
        int totalStands = this.getStandsList().size();
        
        int[] areas = new int[totalStands];
        
        this.getAreaNoRepeats(areas); //fill array with the unique areas within this register
        
        
        //sort array areas
        
        Arrays.sort(areas);
        
//        System.out.println("Areas :" + Arrays.toString(areas));

        //calculating the intervals using the Surges´ rule
        
        //calculate k
        
        double k = this.sturgesRule();
        int classes = (int)Math.round(k);
        
        double range;
        int smallestNumber = this.smallestNumber(areas);
        int largestNumber = this.largestNumber(areas);

        range = ((double)(largestNumber - smallestNumber) / classes);       
//        System.out.println("S : " + smallestNumber);
//        System.out.println("M : " + largestNumber);
//        System.out.println("Classes : " + classes);
//        System.out.println("Range : " + range);
        
        double[][] frequencyTable = new double[classes][5];
        
        //first column will be the lowest number of the interval
        //2 will be the largest number of the interval
        //3 column will be the number of times certain area has appeared on the data set
        //4 will be the relative frequency
        //5 will be the absolute frequency
        
        for(int i=0; i < classes; i++){
                System.out.println(i);
                if(i==0){
                    frequencyTable[0][0] =  (double)smallestNumber - 1;
                    frequencyTable[0][1] =  (smallestNumber + range);
                            for(int s=0; s < range; s++) {
                                   frequencyTable[0][2] += (this.getAreasRepeats((double)(smallestNumber) + s));
                            }
                    frequencyTable[0][3] = frequencyTable[0][2] / totalStands;
                    frequencyTable[0][4] = frequencyTable[0][3];
                }else {

                    frequencyTable[i][0] =  frequencyTable[i-1][1];
                    frequencyTable[i][1] =  frequencyTable[i][0] + range;
                        for(int s=0;s < range ; s++) {
                            frequencyTable[i][2] += this.getAreasRepeats(((frequencyTable[i][0])) + s);
                        }
                    frequencyTable[i][3] = frequencyTable[i][2] / totalStands;
                    frequencyTable[i][4] = frequencyTable[i-1][4] + frequencyTable[i][3];
                }
        }

        
       return frequencyTable;
    }
    
    //method that will fill an given array with the areas contained in this register, without repeating
    private void getAreaNoRepeats(int[] areas) {
        
        int i;
        int x=0;
        String repeatChain = "0;";
        
        for(i=0; i < this.getStandsList().size(); i++){
            if(!repeatChain.contains(Integer.toString(this.getStand(i).getArea()) + ";")){
                repeatChain += Integer.toString(this.getStand(i).getArea()) + ";";
                //System.out.println(areas.length);
                if(x < areas.length) {
                    areas[x] = this.getStand(i).getArea(); 
                    x++;
                }
            }
        }
    }

  
    //method that receives two arrays and uses one to fill another with times that each area on the first repeats within the 
    private double getAreasRepeats(double area) {
        
        double numberRepeats = 0;
             for(int x=0; x < this.getStandsList().size(); x++) {
                 if(this.getStand(x).getArea() == (int)area) {
                     numberRepeats++;
                 }

             }    
             return numberRepeats;
    }
    
    private int smallestNumber(int[] areas) {
        
        int smallestNumber = 1000;
        
        for(int i=0; i < areas.length; i++){
            if(areas[i] < smallestNumber && areas[i]!= 0) {
                smallestNumber = areas[i];
            }
        }
        
        return smallestNumber;
    }
    
    
    private int largestNumber(int[] areas) {
        
         int largestNumber = 0;
        
        for(int i=0; i < areas.length; i++){
            if(areas[i] > largestNumber) {
                largestNumber = areas[i];
            }
        }
        
        return largestNumber;
    } 
    
    //Sturges rule
    private double sturgesRule() {
        
        return 1 + 3.322 * (Math.log10(this.getStandsList().size()));
    }
    
}

