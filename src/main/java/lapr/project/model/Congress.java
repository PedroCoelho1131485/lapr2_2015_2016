/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lapr.project.model;

import java.io.Serializable;

/**
 *
 * @author Pedro<1131485@isep.ipp.pt>
 */
public class Congress extends Event implements Serializable {
    
    private static final long serialVersionUID = 103;
    
    

    public Congress(String title, String description, String beginingDate, String endDate, String placeRealization) {
        super(title, description, beginingDate, endDate, placeRealization);
    }
    
    /**
     * method to return the type of this event
     * @return Congress
     */
    public String getType() {
        
        return "Congress";
    }
    
    /**
     * method that compares type of class with this one
     * @param obj of any type
     * @return true if equals to this object, false if not
     */
    public boolean isSameType(Object obj) {
        
        boolean confirms = false;
        
        if(obj!= null && this.getClass() == obj.getClass()) {
                confirms = true;
            }
        
        return confirms;
    }
    
    /**
     * override equals
     * @param obj Object to be compared
     * @return true if object is the same as this one
     */
    @Override
    public boolean equals(Object obj) {
        
        boolean isSame = false;
       
       if(obj!=null && obj.getClass() == this.getClass()) { 
            Congress c = (Congress) obj; //casting to Congress
            if(this.getTitle().equals(c.getTitle())){
                if(this.getDescription().equals(c.getDescription())) {
                    if(this.getBeginingDate().equals(c.getBeginingDate())){
                        if(this.getEndDate().equals(c.getEndDate())){
                            if(this.getPlaceRealization().equals(c.getPlaceRealization())){
                                isSame = true;
                            }
                        }
                    }
                }
            }
        
       }   
        return isSame;
    }
    
    /**
     * method of override of hashcode
     * @return hashcode of this class
     */
    @Override
    public int hashCode() {
        return 99;
    }

}
