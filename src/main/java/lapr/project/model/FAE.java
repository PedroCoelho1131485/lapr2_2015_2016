package lapr.project.model;

import java.io.Serializable;

/**
 *
 * @author Manuel Sambade
 */
public class FAE extends User implements UserRole, Serializable{
    
    private static final long serialVersionUID = 111;
    
    
        
    /**
     * FAE Constructor 
     * @param name
     * @param email
     * @param username
     * @param password
     */
    public FAE(String name, String email, String username, String password){
        super(name, email, username, password);
    }
    
    
    
      /**
     * method that returns a string that defines the role of this current user
     * @return role of user
     */
    @Override
    public String getRole() {
        
        return UserRole.FAE;
    }
    
    @Override
    public String toString(){
        return String.format("FAE: " + this.getName());
    }
    
     /**
     * equals
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof FAE) {
            FAE otherFAE = (FAE) o;
            return this.getUsername().equals(otherFAE.getUsername());
                    
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }  
}
