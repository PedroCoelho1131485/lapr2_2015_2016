/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;

/**
 *
 * @author Pedro<1131485@isep.ipp.pt>
 */
public class Stand implements Serializable {

    private static final long serialVersionUID = 119;

    /**
     * Atribute description : describe the stand in play
     */
    private String description;

    /**
     * Atribute : area of stand
     */
    private int area;
    
    /**
     * state of the stand : if or isnt already assigned
     */
    private boolean assigned;

    /**
     * constructor that completes the object
     *
     * @param description given by the user
     * @param area given by the user
     */
    public Stand(String description, int area) {

        this.description = description;
        this.area = area;
        this.assigned = false;
    }

    /**
     * constructor default
     */
    public Stand() {
        this.description = "";
        this.area = 0;
        this.assigned = false;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * method that returns the description of the selected Stand
     *
     * @return description
     */
    public String getDescription() {

        return this.description;
    }

    /**
     * method that returns the area of this stand
     *
     * @return this stand area
     */
    public int getArea() {

        return this.area;
    }

    /**
     * method that returns all the information about that Stand
     *
     * @return information of stand
     */
    public String getInformation() {

        return "Stand Info!Description : " + this.getDescription() + "\nwith " + Integer.toString(this.getArea()) + " of area.";
    }

    /**
     * method overried to String
     *
     * @return description of current stand
     */
    @Override
    public String toString() {

        return this.getDescription();
    }
    
    /**
     * returns the state of the stand
     * @return 
     */
    public boolean isStandAssigned() {
        
        return this.assigned;
    }
    
    public void changeStandToAssigned() {
        
        this.assigned = true;
    }

    /**
     * Override of the method hashCode
     *
     * @return description hashcode
     */
    @Override
    public int hashCode() {

        return description.hashCode();
    }

    /**
     * Override of the method equals
     *
     * @param obj general object given by the user
     * @return true if the object recieved is the same as this one, false if not
     */
    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (!(obj instanceof Stand)) {
            return false;
        }

        Stand stand = (Stand) obj;

        return description.equals(stand.description);
    }

}
