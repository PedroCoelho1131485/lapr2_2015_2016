/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Ricardo Pinto
 */
public class DecisionRegister implements Serializable {

    private static final long serialVersionUID = 106;

    /**
     * Contains every decision
     */
    private ArrayList<Decision> decision_list;

    /**
     * Constructor of the class DecisionRegister
     */
    public DecisionRegister() {
        decision_list = new ArrayList<>();
    }

    /**
     * Obtain the list of Decisions
     *
     * @return decision_list
     */
    public ArrayList<Decision> getLstdecioes() {
        return decision_list;
    }

    /**
     * Obtain list size
     *
     * @return size
     */
    public int size() {
        return decision_list.size();
    }

    /**
     * Add decision to the list
     *
     * @param decision
     */
    public void addDecision(Decision decision) {
        decision_list.add(decision);
    }

    /**
     *
     * Obtain index of decision
     *
     * @param decision
     * @return
     */
    public int indexOf(Decision decision) {
        return decision_list.indexOf(decision);
    }

    /**
     *
     * get decision using its index
     *
     * @param index
     * @return
     */
    public Decision getDecision(int index) {
        return decision_list.get(index);
    }

    /**
     *
     * Remove decision from the list
     *
     * @param decision
     */
    public boolean removeDecision(Decision decision) {
        return decision_list.remove(decision);
    }

    /**
     * check if the decision is on the list
     *
     * @param decision
     * @return
     */
    public boolean hasDecision(Decision decision) {
        return decision_list.contains(decision);
    }

    /**
     *
     * @return the list of decision's toString
     */
    public ArrayList<String> decisionLstToString() {
        ArrayList<String> decisionLstString = new ArrayList<>();
        for (Decision d : decision_list) {
            decisionLstString.add(d.toString());
        }
        return decisionLstString;
    }
}
