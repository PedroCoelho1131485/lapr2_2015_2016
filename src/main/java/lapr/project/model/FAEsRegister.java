package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Manuel Sambade
 */
public class FAEsRegister implements Serializable{
    
    private static final long serialVersionUID = 112;
    
    

    /**
     * Contains every FAE
     */
    private ArrayList<FAE> faeList;

    /**
     * Constructor
     */
    public FAEsRegister() {
        this.faeList = new ArrayList<FAE>();
    }

    /**
     * Obtain the list of FAEs
     *
     * @return fae list
     */
    public ArrayList<FAE> getFaeList() {
        return this.faeList;
    }

    /**
     * Obtain a specif FAE by index
     *
     * @param index
     * @return fae
     */
    public FAE getFAE(int index) {
        return faeList.get(index);
    }

    /**
     * Add fae to the list
     *
     * @param fae
     */
    public void addFAE(FAE fae) {
        if (!faeList.contains(fae)) {
            faeList.add(fae);
        }
    }

    /**
     * Remove fae from the list
     *
     * @param fae
     */
    public void removeFAE(FAE fae) {
        if (faeList.contains(fae)) {
            faeList.remove(fae);
        }
    }

    /**
     * Obtain index of fae
     *
     * @param fae
     * @return index
     */
    public int indexOf(FAE fae) {
        return faeList.indexOf(fae);
    }

    /**
     * Obtain list size
     *
     * @return size
     */
    public int getSize() {
        return faeList.size();
    }
    
    public int getFAEByUsername (String username){
        int fae = -1;
        boolean found = false;
        int i=0;
        while(!found && i< this.getSize() ){
            if(username.equals(this.getFaeList().get(i).getUsername())){
               fae = i;
               found = true;
            }
            i++;
        }
        return fae;
    }
}
