/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lapr.project.model;

import java.io.Serializable;

/**
 *
 * @author Pedro<1131485@isep.ipp.pt>
 */
public class Exhibition extends Event implements Serializable{
    
    private static final long serialVersionUID = 110;
    
    
    public Exhibition(String title, String description, String beginingDate, String endDate, String placeRealization) {
        super(title, description, beginingDate, endDate, placeRealization);
    }
    
    /**
     * methods that returns type of this object
     * @return Exhibition
     */
    public String getType() {
        
        return "Exhibition";
    }
    
    /**
     * method that compares type of class with this one
     * @param obj of anykind
     * @return true if equal to this object, false if not
     */
    public boolean isSameType(Object obj) {
        
        boolean confirms = false;
        
        if(obj!= null && this.getClass() == obj.getClass()) {
                confirms = true;
            
        }
        
        return confirms;
    }
    
    /**
     * override equals
     * @param obj Object to be compared
     * @return true if object is the same as this one
     */
    @Override
    public boolean equals(Object obj) {
        
        boolean isSame = false;
        
      if(obj!= null){  
        if(obj.getClass() == this.getClass()) {
            Exhibition ex = (Exhibition) obj; //casting to Congress
            if(this.getTitle() == ex.getTitle()){
                if(this.getDescription() == ex.getDescription()) {
                    if(this.getBeginingDate() == ex.getBeginingDate()){
                        if(this.getEndDate() == ex.getEndDate()){
                            if(this.getPlaceRealization() == ex.getPlaceRealization()){
                                isSame = true;
                            }
                        }
                    }
                }
            }
        }
      }  
        return isSame;
    }
    
    /**
     * method of override of hashcode
     * @return hashcode of this class
     */
    @Override
    public int hashCode() {
        return 98;
    }
    
}
