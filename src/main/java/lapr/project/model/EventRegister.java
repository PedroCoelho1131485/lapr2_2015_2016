/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 *
 * @author Pedro<1131485@isep.ipp.pt>
 */
public class EventRegister implements Serializable {

    private static final long serialVersionUID = 109;

    /**
     * Atribute: Represents the event list
     */
    private ArrayList<Event> eventList;

    /**
     * constructor
     */
    public EventRegister() {
        this.eventList = new ArrayList<Event>();

    }

    /**
     * Obtain the event's list
     *
     * @return
     */
    public ArrayList<Event> getEventList() {
        return this.eventList;
    }

    /**
     * Add event to the list
     *
     * @param e
     */
    public void addEvent(Event e) {
        if (!eventList.contains(e)) {
            this.eventList.add(e);
        }
    }

    /**
     * get the index of a certain event on the list
     *
     * @param x
     * @return
     */
    public int getPosition(Event x) {

        return this.eventList.indexOf(x);
    }

    /**
     * searchs for a specific event on the list
     *
     * @param x
     * @return
     */
    public boolean searchEvent(Event x) {

        return this.eventList.contains(x);
    }

    /**
     * Obtain a certain event by index
     *
     * @param index
     * @return event
     */
    public Event getEvent(int index) {
        return this.eventList.get(index);
    }

    public double getGlobalMeanRate() {
        double sumFaeMeanRate = 0;
        int decisionTotal = 0;
        for (Event e : getEventList()) {
            for (Application a : e.getApplicationRegister().getApplicationList()) {
                for (Decision d : a.getDecisionRegister().getLstdecioes()) {
                    sumFaeMeanRate = sumFaeMeanRate + d.getFinalMean();
                }
            }
        }
        for (Event e : getEventList()) {
            for (Application a : e.getApplicationRegister().getApplicationList()) {
                for (Decision d : a.getDecisionRegister().getLstdecioes()) {
                    decisionTotal++;
                }
            }
        }
        if (decisionTotal == 0) {
            return (double) 0;
        }
        double div = sumFaeMeanRate / decisionTotal;
        return Math.round(div * 100.0) / 100.0;
    }
}
