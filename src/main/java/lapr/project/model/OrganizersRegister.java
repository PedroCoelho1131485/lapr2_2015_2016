/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Pedro<1131485@isep.ipp.pt>
 */
public class OrganizersRegister implements Serializable{
    
    private static final long serialVersionUID = 116;
    

    //atribute
    /**
     * list of organizers only
     */
    private ArrayList<Organizer> organizer_list;
    
    /**
     * constructor
     */
    public OrganizersRegister() {
        this.organizer_list = new ArrayList<Organizer>();
    }
    
    /**
     * add Organizer
     * @param o Organizer give by the user
     */
    public void addOrganizer(Organizer o) {
        if(!this.organizer_list.contains(o)) {
            this.organizer_list.add(o);
        }
    }

    /**
     * get position of a certain Organizer
     * @param o Organizer given by the user
     * @return index of Organizer give
     */
    public int getPosition(Organizer o) {
        
        return this.organizer_list.indexOf(o);
    }
    
    /**
     * removes a Organizer
     * @param o Organizer given by the user
     */
    public void removeOrganizer(Organizer o) {
        
        if(this.organizer_list.contains(o)) {
            this.organizer_list.remove(o);
        }
    }
    
    /**
     * returns the list of organizers
     * @return organizers list
     */
    public ArrayList<Organizer> getOrganizersList() {
        
        return this.organizer_list;
    }
}
