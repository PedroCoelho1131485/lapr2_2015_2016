/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Ricardo Pinto
 */
public class User implements Serializable{
private static final long serialVersionUID = 121;
    /**
     * Represents the name of the user
     */
    private String name;
    /**
     * Represents the email of the user
     */
    private String email;
    /**
     * Represents the username of the user
     */
    private String username;
    /**
     * Represents the hashed password of the user
     */
    private String hashedPassword;

    /**
     *
     * Constructor of the class User
     *
     * @param name of the user, String
     * @param email of the user, String
     * @param UserName of the user,String
     */
    public User(String name, String email, String UserName, String Password) {
        this.name = name;
        this.email = email;
        this.username = UserName;
        this.hashedPassword = encrypt(Password.toCharArray());
        
    }

    /**
     *
     * @param id id given by the user, String
     * @return boolean true if username has the id given by the user
     */
    public boolean hasID(String id) {
        return username.equalsIgnoreCase(id);
    }

    /**
     *
     * @return the username of the user
     */
    public String getID() {
        return username;
    }

    /**
     *
     * @return the name of the user
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name given by the user
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return the email of the user
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email given by the user
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return the username of the user
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username given by the user
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return the hashed password of the user
     */
    public String getHashedPassword() {
        return hashedPassword;
    }

    /**
     *
     * @param hashedPassword that is the hashed password (password is given by
     * the user)
     */
    public void setPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    /**
     * method to return role of user, to be overrided on subclasses
     *
     * @return role of the user
     */
    public String getRole(){
        return UserRole.USER;     
    }
    
    /**
     * 
     * @param o
     * @return 
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof User) {
            User otherUser = (User) o;
            return this.getUsername().equals(otherUser.getUsername());
                    
        }
        return false;
    }

    /**
     * 
     * @return 
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.name);
        hash = 97 * hash + Objects.hashCode(this.email);
        hash = 97 * hash + Objects.hashCode(this.username);
        hash = 97 * hash + Objects.hashCode(this.hashedPassword);
        return hash;
    }

    /**
     *
     * @return toString of the class User
     */
    @Override
    public String toString() {
        return "Name: " + getName() + ", email: " + getEmail() + ", userName: "
                + getUsername() + ", password: " + getHashedPassword();
    }

    public static String encrypt(char[] charPassword) {
        String msg = String.copyValueOf(charPassword);
        int key = 3;
        String s = "";
        int len = msg.length();
        for (int x = 0; x < len; x++) {
            char c = (char) (msg.charAt(x) + key);
            if (c > 'z') {
                s += (char) (msg.charAt(x) - (26 - key));
            } else {
                s += (char) (msg.charAt(x) + key);
            }
        }
        return s;
    }
}
