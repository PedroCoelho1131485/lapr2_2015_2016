/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;

/**
 *
 * @author Manuel Sambade
 */
public class Representative extends User implements UserRole, Serializable{
    
 private static final long serialVersionUID = 118;
    
    public Representative(String name, String email, String UserName, String hashedPassword) {
        super(name, email, UserName, hashedPassword);
    }
    
      /**
     * method that returns a string that defines the role of this current user
     * @return role of user
     */
    @Override
    public String getRole() {
        
        return UserRole.REPRESENTATIVE;
    }
    
}
