/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;

/**
 *
 * @author Ricardo Pinto
 */
public class Participant extends User implements UserRole, Serializable{
    
    private static final long serialVersionUID = 117;
  

    /**
     * Represents the name of the participant
     */
    private String name;
    /**
     * Represents the email of the participant
     */
    private String email;
    /**
     * Represents the username of the participant
     */
    private String username;
    /**
     * Represents the hashedPassword of the participant
     */
    private String hashedPassword;

    /**
     *
     * Constructor of the class Participant
     *
     * @param name of the participant, String
     * @param email of the participant, String
     * @param username
     * @param hashedPassword that is the hashed password (password is given by the user)
     */
    public Participant(String name, String email, String username, String hashedPassword) {
        super(name, email, username, hashedPassword);
    }
    
      /**
     * method that returns a string that defines the role of this current user
     * @return role of user
     */
    @Override
    public String getRole() {
        
        return UserRole.PARTICIPANT;
    }

}
