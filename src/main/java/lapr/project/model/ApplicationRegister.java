/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Ricardo Pinto
 */
public class ApplicationRegister implements Serializable {

    private static final long serialVersionUID = 102;

    /**
     * Contains every application
     */
    private ArrayList<Application> appList;

    /**
     * Constructor of the class ApplicationRegister
     */
    public ApplicationRegister() {
        appList = new ArrayList<>();
    }

    /**
     * Obtain the list of Applications
     *
     * @return app_list
     */
    public ArrayList<Application> getApplicationList() {
        return appList;
    }

    /**
     *
     * Obtain list size
     *
     * @return size
     */
    public int size() {
        return appList.size();
    }

    /**
     * Add application to the list
     *
     * @param app
     */
    public void addApplication(Application app) {
        if (!appList.contains(app)) {
            appList.add(app);
        }
    }

    /**
     *
     * Obtain index of application
     *
     * @param a
     * @return
     */
    public int indexOf(Application a) {
        return appList.indexOf(a);
    }

    /**
     *
     * get application using its index
     *
     * @param index
     * @return
     */
    public Application getApplication(int index) {
        return appList.get(index);
    }

    /**
     *
     * Remove application from the list
     *
     * @param a
     */
    public void removeApplication(Application a) {
        if (appList.contains(a)) {
            appList.remove(a);
        }
    }

    /**
     * method that will get the application that a certain Stand assigned to it
     *
     * @param s Stand
     * @return Stand if found, null if not
     */
    /* public Stand getApplicationWithStand(Stand s) {
        boolean flag = false;
        int i=0;
        while(!flag && i < this.size()) {
            if(this.getApplication(i).getStand() == s) {
                flag = true;
            }
            i++;
        }
        
        if(flag) {
            i--;
            return this.getApplication(i).getStand();
        }
        else return null;
        
     }*/
    /**
     * check if the Application a is on the list
     *
     * @param a
     * @return
     */
    public boolean hasApplication(Application a) {
        return appList.contains(a);
    }

    /**
     *
     * @return the list of application's toString
     */
    public ArrayList<String> lstApplicationToString() {
        ArrayList<String> lstApplicationString = new ArrayList<>();
        for (Application a : appList) {
            lstApplicationString.add(a.toString());
        }
        return lstApplicationString;
    }

    /**
     *
     * @return an array of the applications list
     */
    public Application[] obterArray() {
        return appList.toArray(new Application[appList.size()]);
    }

    public ArrayList<Application> getApplicationOfFae(Event e, String username) {
        ArrayList<Application> appListFAE = new ArrayList<>();
        for (Application a : e.getApplicationRegister().getApplicationList()) {
            for (Decision d : a.getDecisionRegister().getLstdecioes()) {
                if ((d.getDecisionFAE().getUsername()).equals(username)) {
                    appListFAE.add(a);
                }
            }
        }
        return appListFAE;
    }
}
