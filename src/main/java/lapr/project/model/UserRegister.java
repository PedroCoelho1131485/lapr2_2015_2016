/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Ricardo Pinto
 */
public class UserRegister implements Serializable{
    private static final long serialVersionUID = 122;
   
    /**
     * Contains every FAE
     */
    private ArrayList<User> user_list;

    /**
     * Constructor
     */
    public UserRegister() {
        this.user_list = new ArrayList<User>();
    }

    /**
     * Obtain the list of Users
     *
     * @return user list
     */
    public ArrayList<User> getUserList() {
        return this.user_list;
    }

    /**
     * Obtain a specific User by index
     *
     * @param index
     * @return user
     */
    public User getUser(int index) {
        return user_list.get(index);
    }

    /**
     * Add user to the list
     *
     * @param user
     */
    public void addUser(User user) {
        if (!user_list.contains(user)) {
            user_list.add(user);
        }
    }

    /**
     * Remove user from the list
     *
     * @param user
     */
    public void removeFAE(User user) {
        if (user_list.contains(user)) {
            user_list.remove(user);
        }
    }

    /**
     * Obtain index of user
     *
     * @param user
     * @return index
     */
    public int indexOf(User user) {
        return user_list.indexOf(user);
    }

    /**
     * Obtain list size
     *
     * @return size
     */
    public int getSize() {
        return user_list.size();
    }

    /**
     * Gets a certain organizer by a certain username 
     * @param username
     * @return 
     */
    
    public int getOrganizerByUsername(String username) {
        
        boolean found = false;
        
        Organizer o;
        
        int i=0;
        int pos = -1;
        while(!found && i < this.getUserList().size()) {
            if(this.getUser(i).getUsername().equals(username)) {
                found = true;
                o = (Organizer) this.getUser(i);
                pos = i;
            }
        }
     
        return pos;
    }
    
    
    public void printUserList() {
        for (User u : user_list) {
            System.out.println(u);
        }
    }
}
