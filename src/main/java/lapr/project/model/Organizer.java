/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;

/**
 *
 * @author Manuel Sambade
 */
public class Organizer extends User implements UserRole, Serializable{
    
    private static final long serialVersionUID = 115;
    
    private boolean hasEvent;
    
    
    public Organizer(String name, String email, String UserName, String hashedPassword) {
        super(name, email, UserName, hashedPassword);
        
        this.hasEvent = false;
    }
    
      /**
     * method that returns a string that defines the role of this current user
     * @return role of user
     */
    @Override
    public String getRole() {
        
        return UserRole.ORGANIZER;
    }
    
    /**
     * toString
     * @return 
     */
    @Override
    public String toString() {
        
        return this.getName() + " with email " + this.getEmail();
    }
    
    /**
     * get state
     * @return 
     */
    public boolean getState() {
        
        return this.hasEvent;
    }
    
    /**
     * change state of organizer to has event assigned
     */
    public void changeState() {
        
        this.hasEvent = true;
    }    
}
