/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.Serializable;

/**
 *
 * @author Ricardo Pinto
 */
public class Decision implements Serializable{
    
    private static final long serialVersionUID = 104;

    
    /**
     * Represents the fae: creator of the decision
     */
    private FAE fae;

    /**
     * FAE’s knowledge about the	event topic: from 0 to 5.
     */
    private int faeKnowledge;

    /**
     * Application adequacy for the event:	from 0 to 5.
     */
    private int appAdequacy;

    /**
     * Invitations quantity adequacy for the application: from 0 to 5.
     */
    private int invitations;

    /**
     * Overall	recommendation:	0-5.
     */
    private int overall;

    /**
     * Mean of the 4 parameters: fae knowledge, app adequacy, invitations and
     * overall.
     */
    private double finalMean;

    /**
     * Represents the justification of the decision
     */
    private String justification;

    /**
     *
     * Decision's constructor
     *
     * @param fae
     * @param faeKnowledge
     * @param appAdequacy
     * @param invitations
     * @param overall
     * @param justification
     */
    public Decision(FAE fae, int faeKnowledge, int appAdequacy, int invitations, int overall, String justification) {
        this.fae = fae;
        this.faeKnowledge = faeKnowledge;
        this.appAdequacy = appAdequacy;
        this.invitations = invitations;
        this.overall = overall;
        this.justification = justification;
        this.finalMean = calculateFinalMean();

    }

    /**
     * @return the justification of the fae
     */
    public String getJustification() {
        return justification;
    }

    /**
     * @param justification the justification to set
     */
    public void setJustification(String justification) {
        this.justification = justification;
    }

    /**
     * Obtain fae that created the decision
     *
     * @return fae
     */
    public FAE getDecisionFAE() {
        return this.fae;
    }
    
    /**
     * Obtain atribute final mean from the 4 parameters
     * @return finalMean
     */
    public double getFinalMean(){
        return this.finalMean;
    }

    /**
     * Calculates the mean of the 4 parameters for the decision
     *
     * @param faeKnowledge
     * @param appAdequacy
     * @param invitations
     * @param overall
     * @return mean
     */
    public double calculateFinalMean() {
        int sum = this.faeKnowledge + this.appAdequacy + this.invitations + this.overall;
        return (double) sum / 4;
    }
    
    /**
     * method to String
     * @return 
     */
    @Override
    public String toString() {
        
        return this.fae.toString() + " Mean: " + this.getFinalMean() + " with justification: " + this.getJustification();
    }

    /**
     * @param faeKnowledge the faeKnowledge to set
     */
    public void setFaeKnowledge(int faeKnowledge) {
        this.faeKnowledge = faeKnowledge;
    }

    /**
     * @param appAdequacy the appAdequacy to set
     */
    public void setAppAdequacy(int appAdequacy) {
        this.appAdequacy = appAdequacy;
    }

    /**
     * @param invitations the invitations to set
     */
    public void setInvitations(int invitations) {
        this.invitations = invitations;
    }

    /**
     * @param overall the overall to set
     */
    public void setOverall(int overall) {
        this.overall = overall;
    }
    
    /**
     * 
     * @param finalMean the finalMean to set
     */
    public void setFinalMean(double finalMean){
        this.finalMean = finalMean;
    }
}
