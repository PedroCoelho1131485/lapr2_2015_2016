package lapr.project.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Ricardo Pinto
 */
public class Application implements Serializable {

    private static final long serialVersionUID = 101;

//    /**
//     * Represents the name of the company
//     */
//    private String companyName;
//    /**
//     * Represents the address of the company
//     */
//    private String address;
//    /**
//     * Represents the phone number of the company
//     */
//    private int phoneNumber;
    /**
     * Represents the list of decisions
     */
    private DecisionRegister decisionREG;

    /**
     * Keywords list
     *
     */
    private ArrayList<Keyword> keywordList;

    /**
     * Invite quantity
     */
    private int inviteQt;

    /**
     * Application description
     */
    private String description;
    /**
     * Represents the booth Area
     */
    private int boothArea;
    /**
     * Final acceptance decision for application (0 or 1)
     */
    private int finalAcceptance;

    /**
     * stand assigned
     */
    private Stand stand;
    
    /**
     * Acceptance default = -1
     */
    private final int ACCEPTANCE_DEFAULT = -1;

    /**
     *
     * constructor of the class Application
     *
     * @param finalAcceptance
     * @param description
     * @param boothArea
     * @param inviteQt
     */
    public Application(int finalAcceptance, String description, int boothArea, int inviteQt) {
        this.decisionREG = new DecisionRegister();
        this.keywordList = new ArrayList<>();
        this.finalAcceptance = finalAcceptance;
        this.description = description;
        this.boothArea = boothArea;
        this.inviteQt = inviteQt;
        this.stand = new Stand("",0);
    }

    /**
     * Constructor made for submitting application
     *
     * @param description
     * @param boothArea
     * @param inviteQt
     */
    public Application(String description, int boothArea, int inviteQt) {
        this.decisionREG = new DecisionRegister();
        this.keywordList = new ArrayList<>();
        this.finalAcceptance = ACCEPTANCE_DEFAULT;//-1
        this.description = description;
        this.boothArea = boothArea;
        this.inviteQt = inviteQt;
        this.stand = new Stand("",0);
    }

//    /**
//     * Older constructor............
//     *
//     * @param companyName
//     * @param address
//     * @param phoneNumber
//     */
//    public Application(String companyName, String address, int phoneNumber) {
//        this.companyName = companyName;
//        this.address = address;
//        this.phoneNumber = phoneNumber;
//        this.DecisionREG = new DecisionRegister();
//        this.keywordList = new ArrayList<>();
//    }
//    /**
//     * @return the name of the company
//     */
//    public String getCompanyName() {
//        return companyName;
//    }
//
//    /**
//     * @param companyName the companyName to set
//     */
//    public void setCompanyName(String companyName) {
//        this.companyName = companyName;
//    }
//
//    /**
//     * @return the address of the company
//     */
//    public String getAddress() {
//        return address;
//    }
//
//    /**
//     * @param address the address to set
//     */
//    public void setAddress(String address) {
//        this.address = address;
//    }
//
//    /**
//     * @return the phone number of the company
//     */
//    public int getPhoneNumber() {
//        return phoneNumber;
//    }
//
//    /**
//     * @param phoneNumber the phoneNumber to set
//     */
//    public void setPhoneNumber(int phoneNumber) {
//        this.phoneNumber = phoneNumber;
//    }
    /**
     * Obtain description
     *
     * @return description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * @return the inviteQt
     */
    public int getInviteQt() {
        return inviteQt;
    }

    /**
     * Obtain booth area
     *
     * @return booth area
     */
    public int getBoothArea() {
        return this.boothArea;
    }

    /**
     * Obtain number of invites;
     *
     * @return
     */
    public int getInvites() {
        return this.getInviteQt();
    }

    /**
     * Sets a new description
     *
     * @param new_description
     */
    public void setDescription(String new_description) {
        this.description = new_description;
    }

    /**
     * Sets a new booth area
     *
     * @param new_area
     */
    public void setBoothArea(int new_area) {
        this.boothArea = new_area;
    }

    /**
     * Sets a new number of invites
     *
     * @param new_invites
     */
    public void setInvites(int new_invites) {
        this.inviteQt = new_invites;
    }

    /**
     * @return DecisionRegister, List of the decions of an application
     */
    public DecisionRegister getDecisionRegister() {
        return decisionREG;
    }

    /**
     * @param DecisionREG list of Decision
     */
    public void setDecisionRegister(DecisionRegister DecisionREG) {
        this.decisionREG = DecisionREG;
    }

    /**
     * Obtain the list of keywords of the current application
     *
     * @return keyWordList
     */
    public ArrayList<Keyword> getListKeywords() {

        return this.keywordList;
    }

    /**
     *
     * @return Info about current Application
     */
    public String showInfo() {
        String state = "";
        switch (this.getFinalAcceptance()) {

            case 1:
                state = "accepted";
                break;

            case 0:
                state = "rejected";
                break;

            default:
                state = "not decided yet";
                break;

        }
        return description + "\nNumber of Invites : " + this.getInvites() + "\nApplication was " + state;
    }

    /**
     * @return String about Application (current)
     */
    @Override
    public String toString() {

        return String.format("Application: " + description);
    }

    /**
     * Obtain final acceptance for application
     *
     * @return
     */
    public int getFinalAcceptance() {
        return finalAcceptance;
    }
    
    /**
     * method that returns the stand assigned to this application
     * @return 
     */
    public Stand getStand() {
        
        return this.stand;
    }
    
    public boolean hasStandAssigned() {
        
        boolean flag = false;
        
        if(!this.stand.getDescription().equals("") && this.getStand().getArea() != 0) {
            flag = true;
        }
        
        return flag;
    }
    
    /**
     * method that assigns a certain stand to this application
     * @param s 
     */
    public void assignStand(Stand s) {
        
        this.stand = s;
    }

    /**
     * equals
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof Application) {
            Application otherA = (Application) o;
            return this.description.equalsIgnoreCase(otherA.description)
                    && this.boothArea == otherA.boothArea
                    && this.getInviteQt() == otherA.getInviteQt();
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + this.boothArea;
        hash = 41 * hash + this.getInviteQt();
        hash = 41 * hash + Objects.hashCode(this.description);
        return hash;
    }

}
