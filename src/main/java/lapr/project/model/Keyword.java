package lapr.project.model;

import java.io.Serializable;
import java.util.Objects;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Pedro<1131485@isep.ipp.pt>
 */
public class Keyword implements Serializable {
    
    private static final long serialVersionUID = 114;
    
   

    /**
     * value of the keyword
     */
    private String value;
    
    /**
     * complete constructor of this value
     * @param value
     */
    public Keyword (String value) {
        
        this.value = value;
    }

    /**
     * Obtain value of keyword
     * @return value
     */
    public String getValue() {
        
        return this.value;
    }
    
    /**
     * method toString
     * @return 
     */
    @Override
    public String toString() {
        
        return this.getValue();
    }
    
      /**
     * equals
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof Keyword) {
            Keyword otherFAE = (Keyword) o;
            return this.toString().equals(otherFAE.toString());
                    
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.value);
        return hash;
    }
    
    
}
