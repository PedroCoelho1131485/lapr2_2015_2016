/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

/**
 *
 * @author Ricardo Pinto
 */
public interface UserRole {

    public static final String ORGANIZER = "Organizer";
    public static final String REPRESENTATIVE = "Representative";
    public static final String FAE = "Fae";
    public static final String PARTICIPANT = "Participant";
    public static final String EVENTMANAGER = "EventManager";
    public static final String USER = "USER";
    public static final String NOTREG = "Not Registered";
}
