package lapr.project.model;

import java.io.Serializable;

/**
 *
 * @author Manuel Sambade
 */
public class Event  implements Serializable{
    
    private static final long serialVersionUID = 107;
    
    

    /**
     * @return the beginingDate
     */
    public String getBeginingDate() {
        return beginingDate;
    }

    /**
     * @return the endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * @return the applicationRegister
     */
    public ApplicationRegister getApplicationRegister() {
        return applicationRegister;
    }

    /**
     * Atribute title: represents the name of the event
     */
    private String title;

    /**
     * Atribute description: represents what will be happening
     */
    private String description;

    /**
     * Atribute beginingDate: represents the starting date of the event
     */
    private String beginingDate;

    /**
     * Atribute endDate: represents the date when the event will be over
     */
    private String endDate;

    /**
     * Atribute placeRealization: represents the place where the event will be
     * realized
     */
    private String placeRealization;

    /**
     * Atribute applicationList: register class which has the list of
     * Applications
     */
    private ApplicationRegister applicationRegister;

    /**
     * Atribute faeList: register class which has the list of FAEs
     */
    private FAEsRegister faeRegister;

    /**
     * Atribute organizerList : register class which has the list of organizers
     * assigned to each event
     */
    private OrganizersRegister organizerRegister;
    
    /**
     * Atribute standsList : register class which has the list of stands
     */
    private StandsRegister standRegister;

    /**
     * @return title of the current event
     */
    public String getTitle() {

        return this.title;
    }

    /**
     * @return description of the current event
     */
    public String getDescription() {

        return this.description;
    }

    /**
     * @return place of Realization
     */
    public String getPlaceRealization() {

        return this.placeRealization;
    }

    /**
     * Obtain FAEsRegister (that contains the list of FAEs)
     *
     * @return faes register
     */
    public FAEsRegister getFAEsRegister() {
        return this.faeRegister;
    }

    /**
     * method to set a new organizer in a event
     *
     * @param o Organizer to be added
     * @return true if organizer isnt already associated to this event, false if
     * already exist within the register of organizers
     */
    public boolean setOrganizer(Organizer o) {

        boolean added = false;

        //validate organizer
        if (!this.organizerRegister.getOrganizersList().contains(o)) {
            added = true;
            this.getOrganizersRegister().addOrganizer(o);
        }

        return added;
    }

    /**
     * Returns the organizers register of this event
     *
     * @return organizersRegister
     */
    public OrganizersRegister getOrganizersRegister() {

        return this.organizerRegister;
    }
    /**
     * method that sets a title given by the user for the event
     * @param title 
     */
    public void setTitle(String title) {
        
        this.title = title;
    }

    /**
     * Returns the title of the event
     *
     * @return title
     */
    @Override
    public String toString() {
        return this.title;
    }

    /**
     * Obtain event's acceptance rate
     *
     * @return acceptance rate
     */
    public double getAcceptanceRate() {
        int aprooved_counter = 0;
        for (Application a : applicationRegister.getApplicationList()) {

            if (a.getFinalAcceptance()==1) {
                aprooved_counter++;
            }
        }
        if(applicationRegister.size()==0){
            return (double) 0;
        }
        return (double) aprooved_counter / applicationRegister.size();
    }

    /**
     * full constructor of the object Event
     *
     * @param title
     * @param description
     * @param beginingDate
     * @param endDate
     * @param placeRealization
     */
    public Event(String title, String description, String beginingDate, String endDate, String placeRealization) {

        //variables should be all locally validated on User interface, following standards buiseness logic
        this.title = title;
        this.description = description;
        this.beginingDate = beginingDate;
        this.endDate = endDate;
        this.placeRealization = placeRealization;
        this.applicationRegister = new ApplicationRegister();
        this.faeRegister = new FAEsRegister();
        this.organizerRegister = new OrganizersRegister();
        this.standRegister = new StandsRegister();
    }

    /**
     * @param applicationRegister list of application
     */
    public void setApplicationRegister(ApplicationRegister applicationRegister) {
        this.applicationRegister = applicationRegister;
    }
    
    /**
     * Obtain the stands register
     * @return 
     */
    public StandsRegister getStandsRegister() {
        
        return this.standRegister;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @param beginingDate the beginingDate to set
     */
    public void setBeginingDate(String beginingDate) {
        this.beginingDate = beginingDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * @param placeRealization the placeRealization to set
     */
    public void setPlaceRealization(String placeRealization) {
        this.placeRealization = placeRealization;
    }
}
