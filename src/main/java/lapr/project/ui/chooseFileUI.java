/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.ui;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import lapr.project.controller.ImportEventsDataController;
import lapr.project.model.FairCenter;
/**
 *
 * @author Pedro
 */
public class chooseFileUI extends javax.swing.JFrame {
    
    private static final long serialVersionUID = 32;
    
//    private static int optionFileChooser = -1;
    
    private static FairCenter fairCenter;

    /**
     * Creates new form chooseFileUI
     * @param fairCenter
     */
    public chooseFileUI(FairCenter fairCenter) {
        chooseFileUI.fairCenter = fairCenter;

        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fileChooser = new javax.swing.JFileChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Choose an xml file to import Events Data");

        fileChooser.setApproveButtonText("Select");
        fileChooser.setApproveButtonToolTipText("Select file to import  the data from a Event");
        fileChooser.setDialogTitle("Choose a xml file with data from a event to import");
        fileChooser.setToolTipText("Only xml type of files will be accepted");
        fileChooser.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        fileChooser.setName(""); // NOI18N
        fileChooser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileChooserActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(fileChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(fileChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void fileChooserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileChooserActionPerformed
       int returnVal = fileChooser.showOpenDialog(this);
       
        if (returnVal == JFileChooser.APPROVE_OPTION) { //0 for approve , 1 for cancel , only two options
        File file = fileChooser.getSelectedFile();

        String path = file.getPath();

        String pathToReturn;

                 
            if(path.contains("xml")) {

                int resultOption = JOptionPane.showConfirmDialog(null, "Are you sure you want to import the data of this file?","Confirm operation",JOptionPane.YES_NO_OPTION);
                if(resultOption == JOptionPane.YES_OPTION) {
                    pathToReturn = path;

                    System.out.println(pathToReturn);

                    ImportEventsDataController ctrl = new ImportEventsDataController(fairCenter);
                     if(ctrl.importDataEvent(pathToReturn)) {
                
                    JOptionPane.showMessageDialog(null,"All data imported with sucess","Data imported!",JOptionPane.INFORMATION_MESSAGE);
                         System.out.println(fairCenter.getUsersRegister().getUserList().toString());
                    }
                }    
                else {
                    JOptionPane.showMessageDialog(null,"No file path defined yet","Information",JOptionPane.INFORMATION_MESSAGE);

                }
            

                   
                }
            
            else {
                JOptionPane.showMessageDialog(null, "This file isnt xml format. Please choose another","Invalid format file",JOptionPane.ERROR_MESSAGE);

            }
    }
        
        if(returnVal == JFileChooser.CANCEL_OPTION) {
            this.dispose();
        }
        this.dispose();


    }//GEN-LAST:event_fileChooserActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(chooseFileUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(chooseFileUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(chooseFileUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(chooseFileUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new chooseFileUI(fairCenter).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFileChooser fileChooser;
    // End of variables declaration//GEN-END:variables
}
