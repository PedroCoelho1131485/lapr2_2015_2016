package lapr.project.ui;

import javax.swing.JOptionPane;
import lapr.project.controller.CreateEventController;
import lapr.project.model.Event;
import lapr.project.model.EventRegister;
import lapr.project.model.FairCenter;

/**
 *
 * @author Pedro<1131485@isep.ipp.pt>
 */
public class CreateEventUI extends javax.swing.JFrame {

    /**
     * Serial Version Unique Identification Number
     */

    private static final long serialVersionUID = 1;
    
    private static FairCenter fairCenter;
    
    private  CreateEventController ctrl;
    
    /**
     * Creates new form CreateEventUI
     * @param f
     */
    public CreateEventUI(FairCenter f) {
        this.ctrl = new CreateEventController(f);
        initComponents();
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnCreateNewEvent = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        lblTitle = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtTitle = new javax.swing.JTextField();
        lblPlace = new javax.swing.JLabel();
        lblStartDate = new javax.swing.JLabel();
        txtEndDate = new javax.swing.JTextField();
        lblEndDate = new javax.swing.JLabel();
        txtStartDate = new javax.swing.JTextField();
        lblDescription = new javax.swing.JLabel();
        txtPlace = new javax.swing.JTextField();
        lblType = new javax.swing.JLabel();
        radioCongress = new javax.swing.JRadioButton();
        radioExhibition = new javax.swing.JRadioButton();
        lblMsgFB = new javax.swing.JLabel();
        lblFeedBack = new javax.swing.JLabel();
        txtDescription = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Create Event");
        setBackground(new java.awt.Color(153, 153, 153));
        setBounds(new java.awt.Rectangle(500, 200, 0, 0));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setLocation(new java.awt.Point(0, 0));
        setMinimumSize(new java.awt.Dimension(600, 400));
        setSize(new java.awt.Dimension(600, 400));

        btnCreateNewEvent.setFont(new java.awt.Font("Dubai Medium", 0, 11)); // NOI18N
        btnCreateNewEvent.setText("Create New Event");
        btnCreateNewEvent.setName("btnCreateEvent"); // NOI18N
        btnCreateNewEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateNewEventActionPerformed(evt);
            }
        });

        btnCancel.setFont(new java.awt.Font("Dubai Medium", 0, 11)); // NOI18N
        btnCancel.setText("Return");
        btnCancel.setActionCommand("Exit");
        btnCancel.setName("bntExit"); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createCompoundBorder());

        lblTitle.setFont(new java.awt.Font("DialogInput", 0, 11)); // NOI18N
        lblTitle.setText("Title :");
        lblTitle.setName("lblTitle"); // NOI18N

        jLabel2.setFont(new java.awt.Font("Elephant", 0, 11)); // NOI18N
        jLabel2.setText("Fill all the fields with information of the event you want to create");

        txtTitle.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTitle.setName("txtTitle"); // NOI18N
        txtTitle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTitleActionPerformed(evt);
            }
        });

        lblPlace.setFont(new java.awt.Font("DialogInput", 0, 11)); // NOI18N
        lblPlace.setText("Place of Realization :");
        lblPlace.setToolTipText("");
        lblPlace.setName("lblPlace"); // NOI18N

        lblStartDate.setFont(new java.awt.Font("DialogInput", 0, 11)); // NOI18N
        lblStartDate.setText("Date of Start :");
        lblStartDate.setToolTipText("");
        lblStartDate.setName("lblStartDate"); // NOI18N

        txtEndDate.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtEndDate.setName("txtEndDate"); // NOI18N

        lblEndDate.setFont(new java.awt.Font("DialogInput", 0, 11)); // NOI18N
        lblEndDate.setText("Date of End :");
        lblEndDate.setToolTipText("");
        lblEndDate.setName("lblEndDate"); // NOI18N

        txtStartDate.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtStartDate.setName("txtStartDate"); // NOI18N

        lblDescription.setFont(new java.awt.Font("DialogInput", 0, 11)); // NOI18N
        lblDescription.setText("Description :");
        lblDescription.setToolTipText("");
        lblDescription.setName("lblDescription"); // NOI18N

        txtPlace.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtPlace.setName("txtPlace"); // NOI18N

        lblType.setText("Type of Event :");
        lblType.setName("lblType"); // NOI18N

        radioCongress.setText("Congress");
        radioCongress.setName("radioCongress"); // NOI18N
        radioCongress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioCongressActionPerformed(evt);
            }
        });

        radioExhibition.setText("Exhibition");
        radioExhibition.setName("radioExhibition"); // NOI18N
        radioExhibition.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioExhibitionActionPerformed(evt);
            }
        });

        lblMsgFB.setText("Feedback");

        txtDescription.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(138, 138, 138)
                .addComponent(jLabel2)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(110, 110, 110)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblType, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(radioCongress, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(39, 39, 39)
                                .addComponent(radioExhibition, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblMsgFB, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28)
                                .addComponent(lblFeedBack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblPlace)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(lblDescription, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(lblStartDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                                .addComponent(txtStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(24, 24, 24)
                                                .addComponent(lblEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(txtDescription, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 226, Short.MAX_VALUE)
                                                .addComponent(txtPlace, javax.swing.GroupLayout.Alignment.TRAILING)))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTitle)
                    .addComponent(txtTitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPlace)
                    .addComponent(txtPlace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDescription)
                    .addComponent(txtDescription, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblEndDate)
                    .addComponent(txtEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblStartDate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblType)
                    .addComponent(radioCongress)
                    .addComponent(radioExhibition))
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMsgFB)
                    .addComponent(lblFeedBack, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(111, 111, 111)
                .addComponent(btnCreateNewEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 217, Short.MAX_VALUE)
                .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(137, 137, 137))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCreateNewEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCreateNewEventActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateNewEventActionPerformed
        //code for btnCreateEvent
              
        boolean keepGoing;

            keepGoing = validateFields();

        
        if(keepGoing) {
           //proceed to the creation of the event
           
           String type;
           if(radioCongress.isSelected()) {
               type = "Congress";
             } 
           else {
               type = "Exhibition";
           }
           
           String title = txtTitle.getText();
           String description = txtTitle.getText();
           String place = txtPlace.getText();
          
           String dataEvent = type + "\nTitle : " + title + "\nDescription : " + description
                   + "\nPlace of Realization : " + place + "\nConfirm Data?";
           
           int confirmResult = JOptionPane.showConfirmDialog(null, dataEvent);
           int i=0;
           EventRegister er = ctrl.getEvents();
           if(confirmResult == JOptionPane.YES_OPTION) {
               boolean isEqual = false;
               while(!isEqual && i < er.getEventList().size()) {
                   if(er.getEvent(i).getTitle().equals(title) && er.getEvent(i).getPlaceRealization().equals(place)) {
                       isEqual = true;
                   }
                   i++;
               }
               if(!isEqual) {
                    Event e = createEvent();
                    JOptionPane.showMessageDialog(null, "Event created and registed with sucess!");
                    openNewWindow(e);
                    clearInfo();
               }
               else{
                   JOptionPane.showMessageDialog(null,"This Event already exist on the system.\nTry again with different title and place of Realization!","Data Failure",JOptionPane.ERROR_MESSAGE);
                   clearInfo();
               }
           }
       }
      
        
    }//GEN-LAST:event_btnCreateNewEventActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        //exits to the main menu
        this.setVisible(false);
    }//GEN-LAST:event_btnCancelActionPerformed

    private void txtTitleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTitleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTitleActionPerformed

    private void radioCongressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioCongressActionPerformed
        radioExhibition.setSelected(false);
        //makes sure that this radiobutton is the only one selected
    }//GEN-LAST:event_radioCongressActionPerformed

    private void radioExhibitionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioExhibitionActionPerformed
        radioCongress.setSelected(false);
        //makes sure that this radiobutton is the only one selected
    }//GEN-LAST:event_radioExhibitionActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CreateEventUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CreateEventUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CreateEventUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CreateEventUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CreateEventUI(fairCenter).setVisible(true);
            }
        });
    }
    
    //method that clears all the field
    private void clearInfo() {
        //prepares the window to be filled again with the information to create another event
        txtTitle.setText("");
        txtDescription.setText("");
        txtPlace.setText("");
        txtStartDate.setText("");
        txtEndDate.setText("");
        radioCongress.setSelected(false);
        radioExhibition.setSelected(false);
    }
    
    //method to verify all the fields
    private boolean validateFields() {
        boolean validated = false;
        lblFeedBack.setText("");
        
        //prepare variables with the fields information to be more easier and more organized to validated such information
        String title = lblTitle.getText().trim().toLowerCase(); //title of the event 
        String description = lblDescription.getText().trim().toLowerCase();//definition of whats happening on the event, or what the event will represent or show
        String startDate = lblStartDate.getText().trim(); //the begining Date of said event
        String endDate = lblEndDate.getText().trim(); // the date that said event will end
        String place = lblPlace.getText().trim().toLowerCase(); // where the said event will be realized (place of Realization)
        int type = -1; //define the type of the event to be created , 1 for congress , 2 for exhibition
                
        //title of event cant start with a number
        if(title.isEmpty() || Character.isDigit(title.charAt(0))) {
            lblFeedBack.setText("Title field is still empty or value starts with a digit");
        }
        
            else if(description.isEmpty() || Character.isDigit(description.charAt(0))) {
                 lblFeedBack.setText("Description field is still empty  or value starts with a digit");
            }
        
                else if(startDate.isEmpty()) {
                    lblFeedBack.setText("Missing a start Date");
                }
        
                    else if(endDate.isEmpty()) {
                         lblFeedBack.setText("Missing a ending date");
                    }
        
                        else if(place.isEmpty() || Character.isDigit(place.charAt(0))) {
                            lblFeedBack.setText("Place field is still empty  or value starts with a digit");
                         }
        
                            else if(!radioCongress.isSelected() && !radioExhibition.isSelected()) {
                                //at least one radio button must be active to create an event
                                 lblFeedBack.setText("No type of event selected");
                                 
                             }
                            else {
                                    validated = true;

                            }
        return validated;
    }
        
    //method to create event
    private Event createEvent() {
        
        int type;
        
        if(radioCongress.isSelected()) {
            type = 1;
        }
        else {
            type = 2;
        }
        
        
        Event e = ctrl.newEvent(txtTitle.getText().trim(), txtDescription.getText().trim(), txtStartDate.getText().trim(), txtEndDate.getText().trim(), txtPlace.getText().trim(),type);
        
        return e;
    }
    
    //method that will open the new UI to choose the Organizers for the event just created
    private void openNewWindow(Event e) {
        DefineOrganizersUI oWindow = new DefineOrganizersUI(CreateEventUI.fairCenter,this.ctrl,e);
        
        oWindow.setVisible(true);
    } 
    
 
        

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnCreateNewEvent;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblDescription;
    private javax.swing.JLabel lblEndDate;
    private javax.swing.JLabel lblFeedBack;
    private javax.swing.JLabel lblMsgFB;
    private javax.swing.JLabel lblPlace;
    private javax.swing.JLabel lblStartDate;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JLabel lblType;
    private javax.swing.JRadioButton radioCongress;
    private javax.swing.JRadioButton radioExhibition;
    private javax.swing.JTextField txtDescription;
    private javax.swing.JTextField txtEndDate;
    private javax.swing.JTextField txtPlace;
    private javax.swing.JTextField txtStartDate;
    private javax.swing.JTextField txtTitle;
    // End of variables declaration//GEN-END:variables
}
